//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "info.h"
#include "newprg.h"
#include "ui_newprg.h"

void Newprg::copyToDefaultLocation(QString filnamn,
                                   const QString *folderaddress)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup(QStringLiteral(u"Path"));
    QString defaultdownloadlocation =
        settings.value(QStringLiteral(u"defaultdownloadlocation"), QDir::homePath()).toString();
    QString savepath = settings.value(QStringLiteral(u"savepath")).toString();
    QString copypath = settings.value(QStringLiteral(u"copypath")).toString();
    settings.endGroup();
    QString copytoFolder = "";
    QString preferred, quality;

    if(ui->actionCreateFolder->isChecked()) {
        preferred = ui->leMethod->text();
        preferred = preferred.toLower();
        quality = ui->leQuality->text();
        copytoFolder = QDir::toNativeSeparators(copypath + folderaddress + "/" +
                                                preferred + "_" + quality);
        QDir dir(copytoFolder);

        if(!dir.exists()) {
            dir.mkpath(".");
        }
    }

    QString from;

    if(ui->actionDownloadToDefaultLocation->isChecked()) {
        from = defaultdownloadlocation;
    } else {
        from = savepath;
    }

    if(copypath.isEmpty()) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("You have not selected any place to copy the media files.\nPlease select a location before proceeding."));
        msgBox.addButton(QMessageBox::Ok);
        msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
        msgBox.exec();
        return;
    }

    if(ui->actionCreateFolder->isChecked()) {
        from = from + folderaddress + "/" + preferred + "_" + quality;
        copypath = copytoFolder;
    }

    QString old = QDir::toNativeSeparators(from + "/" + filnamn);
    QString ny = QDir::toNativeSeparators(copypath + "/" + filnamn);
    QString ny2 = ny;
    ny2.remove('\'');
    QFile f(ny2);

    if(f.exists()) {
        ui->teOut->setTextColor(QColor("red"));
        ui->teOut->append(tr("A file with the same name already exists. The file will not be copied.") + " (" + filnamn + ")");
        ui->teOut->setTextColor(QColor("black"));
        return;
    }

    if(QFile::copy(old, ny)) {
        ui->teOut->setTextColor(QColor("darkBlue"));
        ui->teOut->append(tr("Copy succeeded ") + "(" + filnamn + ")");
        ui->teOut->setTextColor(QColor("black"));
    } else {
        ui->teOut->setTextColor(QColor("red"));
        ui->teOut->append(tr("Copy failed ") + "(" + filnamn + ")");
        ui->teOut->setTextColor(QColor("black"));
    }

    int langd = old.size();
    QString undertexten = old.replace(langd - 3, 3, "srt");
    /* */
    int langd2 = filnamn.size();
    QString undertextfilen = filnamn.replace(langd2 - 3, 3, "srt");

    if(QFile::exists(undertexten)) {
        langd = ny.size();
        QString ny_undertexten = ny.replace(langd - 3, 3, "srt");

        if(QFile::exists(ny_undertexten)) {
            QFile::remove(ny_undertexten);
        }

        if(QFile::copy(undertexten, ny_undertexten)) {
            ui->teOut->setTextColor(QColor("darkBlue"));
            ui->teOut->append(tr("Copy succeeded ") + "(" + undertextfilen + ")");
            ui->teOut->setTextColor(QColor("black"));
        } else {
            ui->teOut->setTextColor(QColor("red"));
            ui->teOut->append(tr("Copy failed ") + "(" + undertextfilen + ")");
            ui->teOut->setTextColor(QColor("black"));
        }
    }
}

void Newprg::copyToDefaultLocation(QString filnamn,
                                   const QString *folderaddress,
                                   const QString *preferredQuality)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Path");
    QString defaultdownloadlocation =
        settings.value("defaultdownloadlocation", QDir::homePath()).toString();
    QString savepath = settings.value("savepath").toString();
    QString copypath = settings.value("copypath").toString();
    settings.endGroup();
    QString copytoFolder = "";

    if(*preferredQuality != "non") {
        copytoFolder = QDir::toNativeSeparators(copypath + folderaddress + "/" +
                                                preferredQuality);
        copypath = copytoFolder;
        QDir dir(copytoFolder);

        if(!dir.exists()) {
            dir.mkpath(".");
        }
    }

    QString from;

    if(ui->actionDownloadToDefaultLocation->isChecked()) {
        from =
            defaultdownloadlocation + "/" + folderaddress + "/" + preferredQuality;
    } else {
        from = savepath + "/" + folderaddress + "/" + preferredQuality;
    }

    if(copypath.isEmpty()) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("You have not selected any place to copy the media files.\nPlease "
                          "select a location before proceeding."));
        msgBox.addButton(QMessageBox::Ok);
        msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
        msgBox.exec();
        return;
    }

    QString old = QDir::toNativeSeparators(from + "/" + filnamn);
    QString ny = QDir::toNativeSeparators(copypath + "/" + filnamn);
    QString ny2 = ny;
    ny2.remove('\'');
    QFile f(ny2);

    if(f.exists()) {
        ui->teOut->setTextColor(QColor("red"));
        ui->teOut->append(tr("A file with the same name already exists. The file will not be copied.") + " (" + filnamn + ")");
        ui->teOut->setTextColor(QColor("black"));
        return;
    }

    if(QFile::copy(old, ny)) {
        ui->teOut->setTextColor(QColor("darkBlue"));
        ui->teOut->append(tr("Copy succeeded ") + "(" + filnamn + ")");
        ui->teOut->setTextColor(QColor("black"));
    } else {
        ui->teOut->setTextColor(QColor("red"));
        ui->teOut->append(tr("Copy failed ") + "(" + filnamn + ")");
        ui->teOut->setTextColor(QColor("black"));
    }

    int langd = old.size();
    QString undertexten = old.replace(langd - 3, 3, "srt");
    /* */
    int langd2 = filnamn.size();
    QString undertextfilen = filnamn.replace(langd2 - 3, 3, "srt");

    if(QFile::exists(undertexten)) {
        langd = ny.size();
        QString ny_undertexten = ny.replace(langd - 3, 3, "srt");

        if(QFile::copy(undertexten, ny_undertexten)) {
            ui->teOut->setTextColor(QColor("darkBlue"));
            ui->teOut->append(tr("Copy succeeded ") + "(" + undertextfilen + ")");
            ui->teOut->setTextColor(QColor("black"));
        } else {
            ui->teOut->setTextColor(QColor("red"));
            ui->teOut->append(tr("Copy failed ") + "(" + undertextfilen + ")");
            ui->teOut->setTextColor(QColor("black"));
        }
    }
}
