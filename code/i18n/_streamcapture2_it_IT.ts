<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>Info</name>
    <message>
        <location filename="../info.cpp" line="32"/>
        <source> is free software, license </source>
        <translation> è un software gratuito, licenza </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="35"/>
        <source>A graphical shell for </source>
        <translation>streamCapture2 è una shell grafica per </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="37"/>
        <source> and </source>
        <translation> e </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="40"/>
        <source> streamCapture2 handles downloads of video streams.&lt;br&gt;</source>
        <translation> stremcapture2 gestisce i download degli stream video.&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="41"/>
        <source>Many thanks to </source>
        <translation>Molte grazie a </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="42"/>
        <source> for the Italian translation.</source>
        <translation> per la traduzione italiana.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="63"/>
        <source> was created </source>
        <translation> è stato creato </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="64"/>
        <source>by a computer with</source>
        <translation>da un computer con</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="25"/>
        <source>bovirus</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="78"/>
        <location filename="../info.cpp" line="96"/>
        <location filename="../info.cpp" line="197"/>
        <source>Compiled by</source>
        <translation>Compilato con</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="111"/>
        <source>Full version number </source>
        <translation>Numero versione completa </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="193"/>
        <source>Unknown version</source>
        <translation>Versione sconosciuta</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="204"/>
        <source>Unknown compiler.</source>
        <translation>Compilatore sconosciuto.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="219"/>
        <source>Revision</source>
        <translation>Revisione</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="224"/>
        <source>Home page</source>
        <translation>Sito web</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="226"/>
        <source>Source code</source>
        <translation>Codice sorgente</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="228"/>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="231"/>
        <source>Phone: </source>
        <translation>Telefono: </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="245"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="242"/>
        <source>This program uses Qt version </source>
        <translation>Questo programma usa Qt versione </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="243"/>
        <source> running on </source>
        <translation> in esecuzione su </translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../newprg.cpp" line="592"/>
        <location filename="../newprg.cpp" line="616"/>
        <source>Enter your password</source>
        <translation>Inserisci password</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="593"/>
        <location filename="../newprg.cpp" line="617"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation>Gli spazi non sono consentiti. Usa solo i caratteri consentiti dal provider. La password non verrà salvata.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="682"/>
        <source>svtplay-dl crashed.</source>
        <translation>svtplay-dl è crashato.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="688"/>
        <source>Could not stop svtplay-dl.</source>
        <translation>Impossibile fermare svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="696"/>
        <source>svtplay-dl stopped. Exit code </source>
        <translation>svtplay-dl fermato. Codice uscita </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="699"/>
        <source>Delete any files that may have already been downloaded.</source>
        <translation>Elimina tutti i file che potresti aver già scaricato.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="721"/>
        <source>Copy to: </source>
        <translation>Copia in: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="806"/>
        <source>Download to: </source>
        <translation>Download in: </translation>
    </message>
</context>
<context>
    <name>Newprg</name>
    <message>
        <location filename="../about.cpp" line="65"/>
        <source>The version history file is not found.</source>
        <translation>File cronologia versioni non trovato.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="85"/>
        <location filename="../about.cpp" line="111"/>
        <location filename="../about.cpp" line="135"/>
        <source>The license file is not found.</source>
        <translation>File licenza non trovato.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="157"/>
        <source> could not be found. Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install </source>
        <translation> non è stato trovato. 
Per installarlo vai in &quot;Strumenti&quot; -&gt; &quot;Strumento manutenzione&quot; </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="160"/>
        <source>Or install </source>
        <translation>O installa </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="160"/>
        <source> in your system.</source>
        <translation> nel sistema.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="199"/>
        <location filename="../newprg.cpp" line="118"/>
        <source>svtplay-dl is not found in the system path.</source>
        <translation>svtplay-dl non trovato nel path di sistema.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="201"/>
        <location filename="../newprg.cpp" line="120"/>
        <source>You can use svtplay-dl that comes with streamCapture2.</source>
        <translation>Puoi usare svtplay-dl che viene fornito con streamCapture2.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="204"/>
        <location filename="../newprg.cpp" line="85"/>
        <source>svtplay-dl.exe is not found in the system path.</source>
        <translation>svtplay-dl.exe non trovato nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="206"/>
        <location filename="../newprg.cpp" line="87"/>
        <source>You can use svtplay-dl.exe that comes with streamCapture2.</source>
        <translation>Puoi usare svtplay-dl.exe che viene fornito con streamCapture2.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="217"/>
        <source> cannot be found. Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install.</source>
        <translation> non trovato. Va in &quot;Strumenti&quot; -&gt; &quot;Strumento manutenzione&quot; per installarlo.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="223"/>
        <source> cannot be found.</source>
        <translation> non trovato.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="228"/>
        <source>svtplay-dl cannot be found or is not an executable program. Please select svtplay-dl manually.</source>
        <translation>svtplay-dl non è stato trovato o non è un programma eseguibile. 
Seleziona manualmente svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="233"/>
        <source>svtplay-dl.exe cannot be found or is not an executable program. Please select svtplay-dl.exe manually.</source>
        <translation>svtplay-dl.exe non è stato trovato o non è un programma eseguibile. 
Seleziona manualmente svtplay-dl-.exe.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="243"/>
        <source>version </source>
        <translation>versione </translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="64"/>
        <location filename="../coppytodefaultlocation.cpp" line="164"/>
        <source>You have not selected any place to copy the media files.
Please select a location before proceeding.</source>
        <translation>Non hai selezionato alcun percorso dove copiare i file multimediali.
Prima di procedere seleziona un percorso.</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="84"/>
        <location filename="../coppytodefaultlocation.cpp" line="180"/>
        <source>A file with the same name already exists. The file will not be copied.</source>
        <translation>Esiste già un file con lo stesso nome. 
Il file non verrà copiato.</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="91"/>
        <location filename="../coppytodefaultlocation.cpp" line="115"/>
        <location filename="../coppytodefaultlocation.cpp" line="187"/>
        <location filename="../coppytodefaultlocation.cpp" line="207"/>
        <source>Copy succeeded </source>
        <translation>Copia completata </translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="95"/>
        <location filename="../coppytodefaultlocation.cpp" line="119"/>
        <location filename="../coppytodefaultlocation.cpp" line="191"/>
        <location filename="../coppytodefaultlocation.cpp" line="211"/>
        <source>Copy failed </source>
        <translation>Copia fallita </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="31"/>
        <location filename="../downloadall.cpp" line="30"/>
        <location filename="../downloadallepisodes.cpp" line="28"/>
        <location filename="../listallepisodes.cpp" line="28"/>
        <location filename="../sok.cpp" line="34"/>
        <source> cannot be found or is not an executable program.</source>
        <translation> non trovato o non è un programam eseguibile.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="32"/>
        <location filename="../downloadall.cpp" line="31"/>
        <location filename="../downloadallepisodes.cpp" line="29"/>
        <location filename="../listallepisodes.cpp" line="29"/>
        <location filename="../sok.cpp" line="35"/>
        <source>Please click on &quot;Tools&quot; and select svtplay-dl.</source>
        <translation>Fai clic su &quot;Strumenti&quot; e seleziona svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="57"/>
        <location filename="../downloadallepisodes.cpp" line="110"/>
        <source>The request is processed...
Preparing to download...</source>
        <translation>La richiesta è stata elaborata - preparazione download...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="100"/>
        <location filename="../download.cpp" line="104"/>
        <source>The video stream is saved in </source>
        <translation>Lo stream video è stato salvato in </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="116"/>
        <location filename="../downloadall.cpp" line="58"/>
        <location filename="../downloadallepisodes.cpp" line="124"/>
        <source>The default folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation>Impossibile trovare la cartella predefinita per il download degli stream video.
Il download è stato annullato.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="129"/>
        <location filename="../downloadall.cpp" line="71"/>
        <location filename="../downloadall.cpp" line="104"/>
        <location filename="../downloadallepisodes.cpp" line="135"/>
        <source>You do not have the right to save to the folder.
Download is interrupted.</source>
        <translation>Non hai diritti sufficienti per salvare in questa cartella.
Download annullato.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="138"/>
        <location filename="../downloadall.cpp" line="109"/>
        <source>Selected folder to copy to is </source>
        <translation>La cartella selezionata in cui copiare è </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="186"/>
        <location filename="../downloadall.cpp" line="175"/>
        <location filename="../downloadallepisodes.cpp" line="184"/>
        <location filename="../listallepisodes.cpp" line="108"/>
        <location filename="../paytv_create.cpp" line="123"/>
        <location filename="../paytv_edit.cpp" line="148"/>
        <location filename="../sok.cpp" line="113"/>
        <source>Enter your password</source>
        <translation>Inserisci la password</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="245"/>
        <location filename="../downloadall.cpp" line="356"/>
        <location filename="../downloadallepisodes.cpp" line="233"/>
        <source>Removing old files, if there are any...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="264"/>
        <location filename="../download.cpp" line="267"/>
        <source>The download failed. Did you forget to enter username and password? Try another bitrate, or &quot;Auto select&quot;. It often helps.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="311"/>
        <location filename="../download.cpp" line="316"/>
        <source>The download failed </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="185"/>
        <location filename="../listallepisodes.cpp" line="109"/>
        <location filename="../sok.cpp" line="114"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation>Non sono ammessi spazi. Usa solo i caratteri approvati dal provider. La password non verrà salvata.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="210"/>
        <source>Starts downloading: </source>
        <translation>Avvio download: </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="241"/>
        <location filename="../downloadall.cpp" line="352"/>
        <location filename="../downloadallepisodes.cpp" line="227"/>
        <source>Merge audio and video...</source>
        <translation>Unisci audio e video...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="275"/>
        <location filename="../download.cpp" line="283"/>
        <source>Download succeeded </source>
        <translation>Download completato </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="293"/>
        <location filename="../download.cpp" line="298"/>
        <location filename="../downloadall.cpp" line="312"/>
        <location filename="../downloadall.cpp" line="318"/>
        <location filename="../downloadallepisodes.cpp" line="268"/>
        <location filename="../downloadallepisodes.cpp" line="271"/>
        <location filename="../downloadallepisodes.cpp" line="276"/>
        <source>Download completed</source>
        <translation>Download completato</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="331"/>
        <location filename="../downloadallepisodes.cpp" line="295"/>
        <source>No folder is selected</source>
        <translation>Nessuna cartella selezionata</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="348"/>
        <location filename="../download.cpp" line="349"/>
        <location filename="../listallepisodes.cpp" line="52"/>
        <location filename="../listallepisodes.cpp" line="53"/>
        <source>Searching...</source>
        <translation>Ricerca...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="350"/>
        <location filename="../listallepisodes.cpp" line="54"/>
        <source>The request is processed...
Starting search...</source>
        <translation>Richiesta elaborata. 
Avvio ricerca...</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="95"/>
        <source>The default folder for copying video streams cannot be found.
Download is interrupted.</source>
        <translation>Cartella predefinita per copia stream video non trovata. 
Download annullato.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="135"/>
        <location filename="../downloadall.cpp" line="223"/>
        <source>The video streams are saved in </source>
        <translation>Gli stream video sono stati salvati in </translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="255"/>
        <source>Preparing to download</source>
        <translation>Pereparazione al download</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="269"/>
        <location filename="../downloadall.cpp" line="276"/>
        <source>Download </source>
        <translation>Download </translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="269"/>
        <location filename="../downloadall.cpp" line="276"/>
        <source> succeeded (</source>
        <translation> completato (</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="293"/>
        <location filename="../downloadall.cpp" line="298"/>
        <source>The download failed. If a username and password or &apos;st&apos; cookie is required, you must enter these.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="56"/>
        <source>You have chosen to copy to &quot;Default folder&quot;.
Unfortunately, this does not work when you select &quot;Download all episodes&quot;.</source>
        <translation>Hai scelto di copiare nella &quot;Cartella predefinita&quot;.
Qesto non funziona quando selezioni &quot;Scarica tutti gli episodi&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="67"/>
        <source>Because you cannot select &quot;Method&quot; and &quot;Quality&quot; when you select &quot;Download all episodes&quot;, no folders will be created.

Do you want to start the download?</source>
        <translation>Poiché non è possibile selezionare &quot;Metodo&quot; e &quot;Qualità&quot; quando si seleziona &quot;Scarica tutti gli episodi&quot;, non verrà creata alcuna cartella.

Vuoi avviare il download?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="86"/>
        <source>Once svtplay-dl has started downloading all episodes, streamCapture2 no longer has control. If you want to cancel, you may need to log out or restart your computer.
You can try to cancel using the command
&quot;sudo killall python3&quot;

Do you want to start the download?</source>
        <translation>Una volta che svtplay-dl ha iniziato a scaricare tutti gli episodi, streamCapture2 non ha più il controllo. 
Se vuoi annullare il download, potrebbe essere necessario disconnettersi o riavviare il computer.
Puoi provare ad annullare il downlaod usando il comando
&quot;sudo killall python3&quot;

Vuoi avviare il download?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="204"/>
        <source>Starts downloading all episodes: </source>
        <translation>Avvio download di tutti gli episdi: </translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="249"/>
        <source>Episode</source>
        <translation>Episodio</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="249"/>
        <source>of</source>
        <translation>di</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="261"/>
        <source>The media files (and if you have selected the subtitles) have been downloaded.</source>
        <translation>Download file multimediali (e se li hai selezionati i dei sottotitoli) completato.</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="36"/>
        <location filename="../language.cpp" line="75"/>
        <location filename="../language.cpp" line="114"/>
        <source>Restart Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../language.cpp" line="39"/>
        <location filename="../language.cpp" line="78"/>
        <location filename="../language.cpp" line="117"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Affinché le nuove impostazioni della lingua abbiano effetto il programma deve essere riavviato.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="185"/>
        <location filename="../downloadall.cpp" line="174"/>
        <location filename="../language.cpp" line="35"/>
        <location filename="../language.cpp" line="74"/>
        <location filename="../language.cpp" line="113"/>
        <location filename="../newprg.cpp" line="741"/>
        <location filename="../newprg.cpp" line="768"/>
        <location filename="../newprg.cpp" line="977"/>
        <location filename="../newprg.cpp" line="1453"/>
        <location filename="../newprg.cpp" line="1515"/>
        <location filename="../paytv_create.cpp" line="61"/>
        <location filename="../paytv_create.cpp" line="90"/>
        <location filename="../paytv_create.cpp" line="122"/>
        <location filename="../paytv_edit.cpp" line="33"/>
        <location filename="../paytv_edit.cpp" line="114"/>
        <location filename="../paytv_edit.cpp" line="145"/>
        <location filename="../paytv_edit.cpp" line="202"/>
        <location filename="../save.cpp" line="36"/>
        <location filename="../st_create.cpp" line="29"/>
        <location filename="../st_create.cpp" line="37"/>
        <location filename="../st_edit.cpp" line="33"/>
        <location filename="../st_edit.cpp" line="110"/>
        <location filename="../st_edit.cpp" line="124"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="69"/>
        <location filename="../sok.cpp" line="70"/>
        <source>The search field is empty!</source>
        <translation>Il campo ricerca è vuoto!</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="73"/>
        <location filename="../sok.cpp" line="74"/>
        <source>Incorrect URL</source>
        <translation>URL non valida</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="147"/>
        <location filename="../sok.cpp" line="150"/>
        <source> crashed.</source>
        <translation> crashato.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="149"/>
        <location filename="../sok.cpp" line="152"/>
        <source>Can not find any video streams, please check the address.</source>
        <translation>Impossibile trovare uno stream video. 
Verifica l&apos;indirizzo.</translation>
    </message>
    <message>
        <location filename="../st_create.cpp" line="38"/>
        <location filename="../st_edit.cpp" line="125"/>
        <source>Enter &apos;st&apos; cookie</source>
        <translation>Inserisci cookie &apos;st&apos;</translation>
    </message>
    <message>
        <location filename="../st_create.cpp" line="40"/>
        <location filename="../st_edit.cpp" line="128"/>
        <source>Enter the &apos;st&apos; cookie that your video stream provider has saved in your browser.</source>
        <translation>Inserisci il cookie &quot;st&quot; che il provider di streaming video ha salvato nel browser.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="148"/>
        <source>normal</source>
        <translation>normale</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="151"/>
        <source>bold and italic</source>
        <translation>grassetto e corsivo</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="154"/>
        <source>bold</source>
        <translation>grasetto</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="156"/>
        <source>italic</source>
        <translation>corsivo</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="160"/>
        <source>Current font:</source>
        <translation>Font attuale:</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="161"/>
        <source>size:</source>
        <translation>dimensione:</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="287"/>
        <location filename="../setgetconfig.cpp" line="202"/>
        <source>Select &quot;Tools&quot;, &quot;Update&quot; to update.</source>
        <translation>Per aggiornare seleziona &quot;Strumenti&quot; -&gt; &quot;Aggiorna&quot;.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="292"/>
        <location filename="../setgetconfig.cpp" line="207"/>
        <source>Select &quot;Tools&quot;, &quot;Maintenance Tool&quot; and &quot;Update components&quot;.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="295"/>
        <location filename="../setgetconfig.cpp" line="210"/>
        <source>Download a new</source>
        <translation>Scarica un nuovo</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="342"/>
        <source>svtplay-dl is in the system path.</source>
        <translation>svtplay-dl è nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="347"/>
        <source>svtplay-dl.exe is in the system path.</source>
        <translation>svtplay-dl.exe è nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="353"/>
        <source>ERROR! svtplay-dl is not in the system path.</source>
        <translation>ERRORE! svtplay-dl non è nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="357"/>
        <source>ERROR! svtplay-dl.exe is not in the system path.</source>
        <translation>ERRORE! svtplay-dl.exe non è nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="367"/>
        <source>Path to svtplay-dl: </source>
        <translation>percorso svtplay.dl: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="371"/>
        <source>Path to svtplay-dl.exe: </source>
        <translation>Percorso svtplay-dl.exe: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="378"/>
        <source>ERROR! svtplay-dl could not be found.</source>
        <translation>ERRORE! svtplay-dl non trovato.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="382"/>
        <source>ERROR! svtplay-dl.exe could not be found.</source>
        <translation>ERRORE! svtplay-dl.exe non trovato.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="398"/>
        <source>ERROR! FFmpeg cannot be found.</source>
        <translation>ERRORE! ffmpeg.exe non trovato.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="401"/>
        <source>Path to FFmpeg: </source>
        <translation>Percorso FFmpeg: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="406"/>
        <source>FFmpeg can be found in the system path</source>
        <translation>ffmpeg non trovato nel percorso di sistema</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="740"/>
        <location filename="../newprg.cpp" line="767"/>
        <location filename="../newprg.cpp" line="976"/>
        <location filename="../save.cpp" line="35"/>
        <source>Choose</source>
        <translation>Scegli</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="742"/>
        <location filename="../save.cpp" line="37"/>
        <source>Copy streaming media to directory</source>
        <translation>Copia media streaming nella cartella</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="769"/>
        <source>Download streaming media to directory</source>
        <translation>Download file media nella cartella</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="845"/>
        <location filename="../newprg.cpp" line="890"/>
        <source>svtplay-dl cannot be found or is not an executable program. Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install or download AppImage again.</source>
        <translation>svtplay-dl non è stato trovato o non è un programma eseguibile. 
Vai in &quot;Strumenti&quot; -&gt; &quot;Strumento manutenzione&quot; per installare o scaricare nuovamente AppImage.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="856"/>
        <source>svtplay-dl.exe cannot be found or is not an executable program. Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install or download portable again.</source>
        <translation>svtplay-dl.exe non è stato trovato o non è un programma eseguibile. 
Vai in &quot;Strumenti&quot; -&gt; &quot;Strumento manutenzione&quot; per installare o scaricare nuovamente la versione portatile.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="901"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot;
to install or download portable again.</source>
        <translation>svtplay-dl non è stato trovato o non è un programma eseguibile. 
Vai in &quot;Strumenti&quot; -&gt; &quot;Strumento manutenzione&quot;
per installare o scaricare nuovamente AppImage.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="924"/>
        <location filename="../newprg.cpp" line="933"/>
        <source>Cannot find </source>
        <translation>Impossibile trovare </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="924"/>
        <location filename="../newprg.cpp" line="933"/>
        <source> in system path</source>
        <translation> nel percorso di sistema</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="978"/>
        <location filename="../newprg.cpp" line="996"/>
        <source>Select svtplay-dl</source>
        <translation>Seelziona svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="996"/>
        <source>*.exe (svtplay-dl.exe)</source>
        <translation>*.exe (svtplay-dl.exe)</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1012"/>
        <source>svtplay-dl is not an executable program.</source>
        <translation>svtplay-dl non è un programma eseguibile.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1041"/>
        <source> cannot be found or is not an executable program. Click &quot;Tools&quot;, &quot;Select svtplay-dl...&quot;  to select svtplay-dl.</source>
        <translation> non è stato trovato o non è un programma eseguibile. 
Per selezionare svtplay-dl seleziona &quot;Strumenti&quot; -&gt; &quot;Seleziona svtplay-dl ...&quot;.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1106"/>
        <source>Maintenance Tool cannot be found.
Only if you install </source>
        <translation>Strumentio manutenzione non trovato.
Solo se installi </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1108"/>
        <source> is it possible to update and uninstall the program.</source>
        <translation> è possibile aggiornare e disnstallare il programma.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1140"/>
        <source>Failed to create desktop shortcut.
Check your file permissions.</source>
        <translation>Impossibile creare il collegamento sul desktop.
Verifica i tuoi permessi sui file.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1159"/>
        <source>Failed to create shortcut.
Check your file permissions.</source>
        <translation>Impossibile creare il collegamento.
Verifica i tuoi permessi sui file.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1320"/>
        <location filename="../newprg.cpp" line="1321"/>
        <location filename="../newprg.cpp" line="1400"/>
        <source>Auto select</source>
        <translation>Selezione automatica</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1325"/>
        <source>Downloading...</source>
        <translation>Download...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1369"/>
        <source>Can not find any video streams, please check the address.

</source>
        <translation>Impossibile trovare uno stream video. 
Verifica l&apos;indirizzo.

</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1391"/>
        <source>The search is complete</source>
        <translation>Ricerca completata</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1393"/>
        <source>The search failed</source>
        <translation>Ricerca fallita</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1420"/>
        <source>Click to copy to the search box.</source>
        <translation>Fai clic per copiare nel riquadro ricerca.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1443"/>
        <source>The number of previous searches to be saved...</source>
        <translation>Imposta numero precedenti ricerche salvate...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1445"/>
        <source>Specify how many previous searches you want to save. If the number of searches exceeds the specified number, the oldest search is deleted.</source>
        <translation>Specifica quante ricerche precedenti vuoi salvare. 
Se il numero di ricerche supera il numero specificato, la ricerca più vecchia verrà eliminata.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1460"/>
        <source>The number of searches to be saved: </source>
        <translation>Numero precedenti ricerche da salvare: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1470"/>
        <source>Remove all saved searches</source>
        <translation>Rimuovi tutte le ricerche salvate</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1473"/>
        <source>Click to delete all saved searches.</source>
        <translation>Fai clc per eliminare tuitte le ricerche salvate.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1509"/>
        <source>All your saved settings will be deleted. All lists of files to download will disappear. Do you want to continue?</source>
        <translation>Tutte le impostazioni salvate verranno eliminate. 
Tutti gli elenchi file da scaricare verranno azzerati.
Vuoi continuare?</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1563"/>
        <source>Failed to delete your configuration files. Check your file permissions.</source>
        <translation>Impossibile eliminare i file di configurazione. 
Controlla i tuoi permessi sui file.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1578"/>
        <source> was normally terminated. QProsess Exit code = </source>
        <translation> era terminato normalmente. 
QProsess Exit code = </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1583"/>
        <source> crashed. QProsess Exit code = </source>
        <translation> crashato. 
QProsess Exit code = </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="67"/>
        <location filename="../about.cpp" line="87"/>
        <location filename="../about.cpp" line="113"/>
        <location filename="../about.cpp" line="137"/>
        <location filename="../coppytodefaultlocation.cpp" line="66"/>
        <location filename="../coppytodefaultlocation.cpp" line="167"/>
        <location filename="../download.cpp" line="119"/>
        <location filename="../download.cpp" line="132"/>
        <location filename="../download.cpp" line="184"/>
        <location filename="../downloadall.cpp" line="61"/>
        <location filename="../downloadall.cpp" line="74"/>
        <location filename="../downloadall.cpp" line="98"/>
        <location filename="../downloadall.cpp" line="173"/>
        <location filename="../downloadallepisodes.cpp" line="59"/>
        <location filename="../downloadallepisodes.cpp" line="127"/>
        <location filename="../newprg.cpp" line="849"/>
        <location filename="../newprg.cpp" line="858"/>
        <location filename="../newprg.cpp" line="894"/>
        <location filename="../newprg.cpp" line="903"/>
        <location filename="../newprg.cpp" line="926"/>
        <location filename="../newprg.cpp" line="935"/>
        <location filename="../newprg.cpp" line="1014"/>
        <location filename="../newprg.cpp" line="1045"/>
        <location filename="../newprg.cpp" line="1110"/>
        <location filename="../newprg.cpp" line="1452"/>
        <location filename="../newprg.cpp" line="1566"/>
        <location filename="../paytv_create.cpp" line="60"/>
        <location filename="../paytv_create.cpp" line="89"/>
        <location filename="../paytv_create.cpp" line="121"/>
        <location filename="../paytv_edit.cpp" line="113"/>
        <location filename="../paytv_edit.cpp" line="144"/>
        <location filename="../paytv_edit.cpp" line="201"/>
        <location filename="../setgetconfig.cpp" line="375"/>
        <location filename="../setgetconfig.cpp" line="386"/>
        <location filename="../setgetconfig.cpp" line="475"/>
        <location filename="../shortcuts.cpp" line="72"/>
        <location filename="../shortcuts.cpp" line="147"/>
        <location filename="../st_create.cpp" line="28"/>
        <location filename="../st_create.cpp" line="36"/>
        <location filename="../st_edit.cpp" line="109"/>
        <location filename="../st_edit.cpp" line="123"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="189"/>
        <location filename="../downloadall.cpp" line="178"/>
        <location filename="../paytv_create.cpp" line="93"/>
        <location filename="../paytv_create.cpp" line="126"/>
        <location filename="../paytv_edit.cpp" line="118"/>
        <location filename="../paytv_edit.cpp" line="150"/>
        <source>Spaces are not allowed. Use only the characters
your streaming provider approves.</source>
        <translation>Gli spazi non sono consentiti.
Usa solo caratteri approvati dal provider.</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="62"/>
        <location filename="../paytv_edit.cpp" line="203"/>
        <location filename="../st_create.cpp" line="30"/>
        <location filename="../st_edit.cpp" line="111"/>
        <source>Streaming service</source>
        <translation>Servizio streaming</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="64"/>
        <location filename="../paytv_edit.cpp" line="205"/>
        <location filename="../st_create.cpp" line="32"/>
        <location filename="../st_edit.cpp" line="114"/>
        <source>Enter the name of your streaming service.</source>
        <translation>Inserisci il nome del servizio streaming.</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="91"/>
        <location filename="../paytv_edit.cpp" line="116"/>
        <source>Enter your username</source>
        <translation>Inserisci nome utente</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="135"/>
        <location filename="../paytv_edit.cpp" line="160"/>
        <source>Save password?</source>
        <translation>Vuoi salvare la password?</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="137"/>
        <location filename="../paytv_edit.cpp" line="162"/>
        <source>Do you want to save the password? (unsafe)?</source>
        <translation>Vuoi salvare la password (non sicuro)?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="73"/>
        <location filename="../downloadallepisodes.cpp" line="93"/>
        <location filename="../newprg.cpp" line="1514"/>
        <location filename="../paytv_create.cpp" line="141"/>
        <location filename="../paytv_edit.cpp" line="166"/>
        <source>Yes</source>
        <translation>Sì</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="74"/>
        <location filename="../downloadallepisodes.cpp" line="94"/>
        <location filename="../paytv_create.cpp" line="142"/>
        <location filename="../paytv_edit.cpp" line="167"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="28"/>
        <source>Manage Login details for </source>
        <translation>Gestione accessi per </translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="29"/>
        <source>Edit, rename or delete
</source>
        <translation>Modifica, rinomina o elimina
</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="30"/>
        <location filename="../st_edit.cpp" line="31"/>
        <source>Edit</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="31"/>
        <source>Rename</source>
        <translation>Rinomina</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="32"/>
        <location filename="../st_edit.cpp" line="32"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="59"/>
        <source>Create New</source>
        <translation>Crea nuovo</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="67"/>
        <location filename="../setgetconfig.cpp" line="315"/>
        <source>No Password</source>
        <translation>Nessuna password</translation>
    </message>
    <message>
        <location filename="../qsystemtrayicon.cpp" line="27"/>
        <source>The mission failed!</source>
        <translation>Missione fallita!</translation>
    </message>
    <message>
        <location filename="../qsystemtrayicon.cpp" line="30"/>
        <source>Mission accomplished!</source>
        <translation>Missione compiuta!</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="41"/>
        <source>Update</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="42"/>
        <source>Update this AppImage to the latest version</source>
        <translation>Aggiorna questa applicazione alla versione più recente</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="243"/>
        <source>Up and running</source>
        <translation>Attiva ed in esecuzione</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="373"/>
        <source>FFmpeg cannot be found or is not an executable program. Please reinstall the program.
Or install FFmpeg in the system path.</source>
        <translation>FFmpeg non è stato trovato o non è un programma eseguibile. 
Reinstalla il programma.
O installa FFmpeg nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="382"/>
        <source>FFmpeg cannot be found or is not an executable program. Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install or download portable again.
Or install FFmpeg in the system path.</source>
        <translation>FFmpeg non è stato trovato o non è un programma eseguibile. 
Seleziona &quot;Strumenti&quot; -&gt; &quot;Strumento manutenzione&quot; per installare o scaricare la versione portatile.
O installa FFmpeg nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="472"/>
        <source>Could not save a file to store Recent Search list. Check your file permissions.</source>
        <translation>Impossibile salvare il file con l&apos;elenco delle ricerche recenti. 
Controlla i tuoi permessi aui file.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="494"/>
        <source>Could not save a file to store the list off downloads. Check your file permissions.</source>
        <translation>Impossibile salvare il file con l&apos;elenco dei download. 
Controlla i tuoi permessi aui file.</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="70"/>
        <source>Failure!
The shortcut could not be created in
&quot;~/.local/share/applications&quot;
Check your file permissions.</source>
        <translation>Errore! Impossibile creare il collegamento in
&quot;~ / .local / share / applications&quot;
Controlla i tuoi permessi sui file.</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="118"/>
        <location filename="../shortcuts.cpp" line="120"/>
        <source>Download video streams.</source>
        <translation>Download stream video.</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="145"/>
        <source>Failure!
The shortcut could not be created.
Check your file permissions.</source>
        <translation>Errore! Impossibile creare il collegamento.
Verifica i tuoi permessi sui file.</translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="40"/>
        <source>Compiled language file (*.qm)</source>
        <translation>Compila fiile lingua (*.qm)</translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="40"/>
        <source>Open your language file</source>
        <translation>Apri il file lingua</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="26"/>
        <source>Manage &apos;st&apos; cookie for </source>
        <translation>Gestione cookie &apos;st&apos; per </translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="28"/>
        <source>Use, Do not use, Edit or Delete
&apos;st&apos; cookie for
</source>
        <translation>Usa, non usare, modifica o elimina
cookie &apos;st&apos; per
</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="29"/>
        <source>Use</source>
        <translation>Usa</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="30"/>
        <source>Do not use</source>
        <translation>Non usare</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="175"/>
        <source>Set new &apos;st&apos; cookie</source>
        <translation>Imposta nuovo cookie &apos;st&apos;</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="176"/>
        <source>Paste and save the &apos;st&apos; cookie that your streaming provider has downloaded to your browser.</source>
        <translation>Incolla e salva il cookie &apos;st&apos; che il provider dello streaming ha scaricato nel browser.</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="41"/>
        <source>The information from svtplay-dl may or may not contain:</source>
        <translation>L&apos;informazione da svtplay-dl può o non può contenere:</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="42"/>
        <source>Quality, Method, Codec, Resolution, Language and Role</source>
        <translation>Metodo, qualità, codec, risoluzione, lingua e ruolo</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="191"/>
        <source>ERROR: No videos found. Cant find video id for the video.</source>
        <translation>ERRORE: nessun video trovato - impossibile trovare l&apos;ID video  per il video.</translation>
    </message>
    <message>
        <location filename="../zoom.cpp" line="33"/>
        <source>The font size changes to the selected font size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>newprg</name>
    <message>
        <location filename="../newprg.ui" line="32"/>
        <source>TEST</source>
        <translation>TEST</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="62"/>
        <source>Paste the link to the page where the video is displayed</source>
        <translation>Incolla il collegamento alla pagina dove viene visualizzato il video</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="65"/>
        <location filename="../newprg.ui" line="856"/>
        <source>Paste</source>
        <translation>Incolla</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="111"/>
        <location filename="../newprg.ui" line="847"/>
        <source>Search for video streams.</source>
        <oldsource>Search for video files.</oldsource>
        <translation>Cerca stream video.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="114"/>
        <location filename="../newprg.ui" line="844"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="143"/>
        <source>Add current video to the list of files that will be downloaded.</source>
        <translation>Aggiungi video attuale all&apos;elenco di file che verranno scaricati.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="146"/>
        <location filename="../newprg.ui" line="1021"/>
        <source>Add to Download list</source>
        <translation>Aggiungi ad elenco download</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="224"/>
        <source>The number of bits that are conveyed or processed per unit of time. Higher numbers give better quality and larger file..</source>
        <translation>Il numero di bit trasmessi o elaborati per unità di tempo. 
Numeri più alti indicano una qualità migliore ma creano file più grandi. .</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="233"/>
        <source>Quality (Bitrate)</source>
        <oldsource>Bitrate</oldsource>
        <translation>Qualità (bitrate)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="287"/>
        <source>Media streaming communications protocol.</source>
        <translation>Protocollo comunicazione streaming media.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="302"/>
        <source>Method</source>
        <translation>Metodo</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="355"/>
        <source>Searches for the subtitle and downloads it at the same time as the video stream.</source>
        <translation>Cerca il sottotitolo e lo scarica contemporaneamente allo stream video.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="358"/>
        <location filename="../newprg.ui" line="998"/>
        <source>Include Subtitle</source>
        <translation>Includi sottotitoli</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="393"/>
        <source>Download all files you added to the list.</source>
        <translation>Scarica tutti i file aggiunti all&apos;elenco.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="396"/>
        <location filename="../newprg.ui" line="1036"/>
        <source>Download all</source>
        <translation>Scarica tutto</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="434"/>
        <source>Download the file you just searched for</source>
        <translation>Scarica il file che hai appena cercato</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="437"/>
        <location filename="../newprg.ui" line="871"/>
        <source>Download</source>
        <translation>Scarica</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="450"/>
        <source>Select quality on the video you download</source>
        <translation>Seleziona la qualità del video da scaricare</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="459"/>
        <source>Quality (bitrate) and method. Higher bitrate gives better quality and larger file.</source>
        <translation>Qualità (bitrate) e metodo. 
Un bitrate più alto offre una qualità migliore ma crea un file più grande.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="495"/>
        <source>Select quality on the video you download.</source>
        <translation>Seleziona la qualità del video da scaricare.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="526"/>
        <source>Select provider. If yoy need a password.</source>
        <translation>Seleziona provider. 
Se hai bisogno di una password..</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="539"/>
        <location filename="../newprg.ui" line="986"/>
        <location filename="../newprg.ui" line="1106"/>
        <source>If no saved password is found, click here.</source>
        <translation>Se non viene trovata alcuna password salvata, fai clic qui.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="542"/>
        <location filename="../newprg.ui" line="983"/>
        <location filename="../newprg.ui" line="1103"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="622"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="634"/>
        <source>&amp;Language</source>
        <translation>&amp;Lingua</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="643"/>
        <source>&amp;Tools</source>
        <translation>S&amp;trumenti</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="677"/>
        <source>&amp;Help</source>
        <translation>&amp;Guida</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="693"/>
        <source>&amp;Recent</source>
        <oldsource>Recent</oldsource>
        <translation>&amp;Recenti</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="698"/>
        <source>&amp;Download list</source>
        <translation>Elenco &amp;download</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="720"/>
        <source>&amp;Several episodes</source>
        <translation>&amp;Episodi diversi</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="729"/>
        <source>&apos;&amp;st&apos; cookies</source>
        <translation>Cookie &apos;&amp;st&apos;</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="734"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="758"/>
        <source>English</source>
        <translation>Inglese</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="767"/>
        <source>Swedish</source>
        <translation>Svedese</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="775"/>
        <source>Check for updates at program start</source>
        <translation>Controlla aggiornamenti ad avvio programma</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="778"/>
        <source>Check for software updates each time the program starts.</source>
        <oldsource>Check for software updates each time the program starts</oldsource>
        <translation>Controlla la disponibilità di aggironamenti ad ogni avvio del programma.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="793"/>
        <source>About...</source>
        <translation>Info su streamCapture2...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="802"/>
        <source>Check for updates...</source>
        <translation>Controllo aggiornamenti...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="811"/>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="814"/>
        <source>Exits the program.</source>
        <translation>Esci dal programma.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="817"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="826"/>
        <source>About svtplay-dl...</source>
        <translation>Info su svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="835"/>
        <source>About FFmpeg...</source>
        <translation>Info su FFmpeg...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="859"/>
        <source>Paste the link to the page where the video is displayed.</source>
        <translation>Incolla il collegamento alla pagina dove viene visualizzato il video.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="874"/>
        <source>Download the stream you just searched for.</source>
        <oldsource>Download the file you just searched for.</oldsource>
        <translation>Scarica lo stream che hai appena cercato.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="892"/>
        <source>License svtplay-dl...</source>
        <translation>Licenza svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="901"/>
        <source>License FFmpeg...</source>
        <translation>Licenza FFmpeg...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="906"/>
        <source>Recent files</source>
        <translation>File recenti</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="915"/>
        <source>Help...</source>
        <translation>Guida in linea...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="924"/>
        <source>View Download list</source>
        <translation>Visualizza elenco download</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="927"/>
        <source>Look at the list of all the streams to download.</source>
        <oldsource>Look at the list of all the files to download.</oldsource>
        <translation>Visualizza l&apos;elenco di tutti gli stream da scaricare.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="936"/>
        <location filename="../newprg.ui" line="944"/>
        <source>Delete download list</source>
        <translation>Elimina elenco download</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="939"/>
        <source>All saved streams in the download list are deleted.</source>
        <oldsource>All saved items in the download list are deleted.</oldsource>
        <translation>Tutti gli stream salvati nell&apos;elenco download sono stati eliminati.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="953"/>
        <source>Italian</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="962"/>
        <source>Version history...</source>
        <translation>Cronologia versioni...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1369"/>
        <source>Set new &apos;st&apos; cookie</source>
        <translation>Imposta nuovo cookie &apos;st&apos;</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1374"/>
        <source>Edit settings (advanced)</source>
        <translation>Modifica impostazioni (avanzate)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1377"/>
        <source>Open the configuration file with the system text editor.</source>
        <translation>Apre il file configurazione con l&apos;editor testo di sistema.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1386"/>
        <source>Download svtplay-dl...</source>
        <translation>Download svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1389"/>
        <source>Download and decompress svtplay-dl from bin.ceicer.com</source>
        <translation>Scarica e decomprime svtplay-dl da bin.ceicer.com</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1394"/>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1397"/>
        <source>Increase the font size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1400"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1405"/>
        <source>Zoom Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1408"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1413"/>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1416"/>
        <source>Decrease the font size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1419"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="974"/>
        <source>Save the name of a video stream provider, your username and, if you want, your password.</source>
        <translation>Salva il nome di un provider di streaming video, il nome utente e, se lo desideri, la password.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="565"/>
        <source>Allows given quality to differ by an amount. 300 usually works well.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1001"/>
        <source>Searching for and downloading subtitles.</source>
        <translation>Ricerca e download di sottotitoli.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1009"/>
        <source>Explain what is going on</source>
        <translation>Descrivi cosa sta succedendo</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1024"/>
        <source>Add current video to the list of streams that will be downloaded.</source>
        <oldsource>Add current video to the list of files that will be downloaded.</oldsource>
        <translation>Aggiungi il video attuale all&apos;elenco degli stream che verranno scaricati.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1039"/>
        <source>Download all the streams in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</source>
        <oldsource>Download all the files in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</oldsource>
        <translation>Scarica tutti gli stream nell&apos;elenco. 
Se si tratta dello stesso stream video in qualità diverse, verranno engono create automaticamente per ogni flusso video.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1047"/>
        <source>Create folder &quot;method_quality&quot;</source>
        <translation>Crea cartella &quot;method_quality&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1050"/>
        <source>Automatically create a folder for each downloaded video stream.</source>
        <translation>Crea automaticamente una cartella per ogni stream video scaricato.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1059"/>
        <source>Edit Download list (Advanced)</source>
        <translation>Modifica elenco download (avanzato)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1062"/>
        <source>Change method or quality. Remove a download from the list. NOTE! If you change incorrectly, it will not work.</source>
        <translation>Cambia metodo o qualità. 
Rimuovi un download dall&apos;elenco. 
NOTA! Se cambi in modo errato, non funzionerà.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1074"/>
        <source>Save Download list (Advanced)</source>
        <translation>Salva elenco download (avanzato)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1077"/>
        <source>Your changes to the download list are saved.</source>
        <translation>Salvataggio completato modifiche elenco download.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1085"/>
        <source>Show more</source>
        <translation>Visualizza altro</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1088"/>
        <source>View more information from svtplay-dl.</source>
        <translation>Visualizza maggiori informazioni con svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1111"/>
        <source>Uninstall streamCapture</source>
        <translation>Disinstalla streamCapture2</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1114"/>
        <source>Uninstall and remove all components</source>
        <translation>Disinstalla e rimuovi tutti i componenti</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1122"/>
        <source>Direct Download of all Video Streams in current serie (Not from the Download List)</source>
        <translation>Download diretto tutti gli strem video serie attuale (non da elenco download)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1125"/>
        <source>Searches for all Video Streams in the current series and tries to download them directly.</source>
        <translation>Cerca tutti gli stream video della serie attuale e prova a scaricarli direttamente.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1130"/>
        <source>Download after Date...</source>
        <translation>Download dopo una data...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1139"/>
        <source>Stop all downloads</source>
        <translation>Stop di tutti i download</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1142"/>
        <source>Trying to stop svtplay-dl.</source>
        <translation>Prova a fermare svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1147"/>
        <source>List all Video Streams</source>
        <translation>Elenco di tutti gli stream video</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1150"/>
        <source>Looking for Video Streams in the current series.</source>
        <translation>Ricerca stream video nella serie attuale.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1155"/>
        <location filename="../newprg.ui" line="1255"/>
        <source>Delete all settings and Exit</source>
        <translation>Elimina tutte le impostazioni ed esci</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1158"/>
        <source>All saved searches and the list of streams to be downloaded will be deleted.</source>
        <translation>Tutte le ricerche salvate e l&apos;elenco degli stream da scaricare verranno eliminati.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1166"/>
        <source>Copy to Selected Location</source>
        <translation>Copia nel percorso selezionato</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1181"/>
        <source>Select Copy Location...</source>
        <translation>Seleziona percorso in cui copiare...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1184"/>
        <source>Save the location where the finished video file is copied.</source>
        <translation>Salva il percorso in cui viene copiato il file video a download completato.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1193"/>
        <source>Select Default Download Location...</source>
        <translation>Seleziona percorso predefinito download...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1196"/>
        <source>Save the location for direct download.</source>
        <translation>Salva impostazione percorso download diretto.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1204"/>
        <source>Download to Default Location</source>
        <translation>Download nel percorso predefinito</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1207"/>
        <source>Direct download to the default location.</source>
        <translation>Download diretto nel percorso predefinito.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1215"/>
        <source>Add all Video Streams to Download List</source>
        <translation>Aggiungi tutti gli stream video nell&apos;elenco download</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1218"/>
        <source>Searches for all Video Streams in the current series and adds them to the download list.</source>
        <translation>Cerca tutti gli stream video nella serie attuale e aggiungili all&apos;elenco download.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1227"/>
        <source>Select font...</source>
        <translation>Seleziona font...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1236"/>
        <source>Visit svtplay-dl forum for issues...</source>
        <translation>Visita forum svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1244"/>
        <source>Maintenance Tool...</source>
        <translation>Strumento manutenzione...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1247"/>
        <source>Starts the Maintenance Tool. To update or uninstall.</source>
        <translation>Avvia lo strumento di manutenzione per aggiornamenti o disinstallazione.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1258"/>
        <source>All saved searches, download list and settings are deleted.</source>
        <translation>Tutte le ricerche salvate, l&apos;elenco download e le impostazioni verranno eliminate.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1266"/>
        <source>Use the latest svtplay-dl snapshot</source>
        <translation>Usa la versione snapshot più recente di svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1269"/>
        <source>Latest snapshot Use it on your own risk.</source>
        <translation>Versione snapshot più recente di svtplay.-dl. Usala a tuo rischio.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1280"/>
        <source>Use latest stable svtplay-dl</source>
        <translation>Usa la versione stabile più recente di svtplaty-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1283"/>
        <source>Latest Stable Release.</source>
        <translation>Versione stabile più recente di svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1291"/>
        <source>Use svtplay-dl from the system path</source>
        <translation>Usa svtplay-dl dal percorso di sistema</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1294"/>
        <source>Uses (if available) svtplay-dl in the system path.</source>
        <translation>Usa (se disponibile) svtplay-dl nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1303"/>
        <source>Select svtplay-dl...</source>
        <translation>Seleziona svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1311"/>
        <source>Use the selected svtplay-dl</source>
        <translation>Usa svtplay-dl selezionato</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1319"/>
        <source>Do not show notifications</source>
        <translation>Non visualizzare le notifiche</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1322"/>
        <source>Do not show notifications when the download is complete.</source>
        <translation>Non visualizzare le notifiche a download completato.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1334"/>
        <source>Create a shortcut</source>
        <translation>Crea un collegamento</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1345"/>
        <source>Desktop Shortcut</source>
        <translation>Collegamento sul desktop</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1348"/>
        <source>Create a shortcut to streamCapture2 on the desktop.</source>
        <translation>Crea collegamento streamCapture2 sul desktop.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1359"/>
        <source>Create a shortcut to streamCapture2 in the operating system menu.</source>
        <translation>Crea collegamento streamCapture2 nel menu del sistema operativo.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1356"/>
        <source>Applications menu Shortcut</source>
        <translation>Collegamentto menu applicazioni</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="711"/>
        <source>&amp;Login</source>
        <translation>&amp;Accedi</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="883"/>
        <source>License streamCapture2...</source>
        <translation>Licenza streamCapture2...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="971"/>
        <source>Create new user</source>
        <translation>Crea nuovo utente</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1364"/>
        <source>Load external language file...</source>
        <translation>Carica file esterno lingua...</translation>
    </message>
</context>
</TS>
