//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "checkupdate.h"
#include "info.h"
#include "newprg.h"
#include "ui_newprg.h"
#include <QMessageBox>
void Newprg::downloadAll()
{
    if(!fileExists(svtplaydl)) {
        ui->teOut->setTextColor(QColor("red"));
        ui->teOut->setText(svtplaydl +
                           tr(" cannot be found or is not an executable program."));
        ui->teOut->append(tr("Please click on \"Tools\" and select svtplay-dl."));
        ui->teOut->setTextColor(QColor("black"));
        return;
    }

    QString amount = ui->comboAmount->currentText();
    QStringList ARG;
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    avbrutet = false;
    ui->teOut->setTextColor(QColor("black"));
    QString save_all_path;
    settings.beginGroup(QStringLiteral(u"Path"));
    QString defaultdownloadlocation =
        settings.value(QStringLiteral(u"defaultdownloadlocation")).toString();
    QString copypath = settings.value(QStringLiteral(u"copypath")).toString();
    settings.endGroup();

    if(ui->actionDownloadToDefaultLocation->isChecked()) {
        /* */
        const QFileInfo downloadlocation(
            QDir::toNativeSeparators(defaultdownloadlocation));

        if(!downloadlocation.exists()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("The default folder for downloading video streams cannot be "
                              "found.\nDownload is interrupted."));
            msgBox.addButton(QMessageBox::Ok);
            msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
            msgBox.exec();
            ui->teOut->clear();
            return;
        }

        if(!downloadlocation.isWritable()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("You do not have the right to save to the "
                              "folder.\nDownload is interrupted."));
            msgBox.addButton(QMessageBox::Ok);
            msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
            msgBox.exec();
            ui->teOut->clear();
            return;
        }

        /* */
        save_all_path = defaultdownloadlocation;
    } else {
        save_all_path = save();
    }

    QString kopierastill;

    if(ui->actionCopyToDefaultLocation->isChecked()) {
        const QFileInfo copypathlocation(QDir::toNativeSeparators(copypath));

        if(!copypathlocation.exists()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("The default folder for copying video streams "
                              "cannot be found.\nDownload is interrupted."));
            msgBox.addButton(QMessageBox::Ok);
            msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
            msgBox.exec();
            ui->teOut->clear();
            return;
        } else if(!copypathlocation.isWritable()) {
            QMessageBox::critical(this, DISPLAY_NAME " " VERSION,
                                  tr("You do not have the right to save to the "
                                     "folder.\nDownload is interrupted."));
            ui->teOut->clear();
            return;
        } else {
            kopierastill = tr("Selected folder to copy to is ") + "\"" +
                           QDir::toNativeSeparators(copypath + "\"");
        }
    }

    if(save_all_path != "nothing") {
//        QString verbose;
//        verbose = " -v ";
        ui->teOut->setReadOnly(true);
        ui->actionSaveDownloadList->setEnabled(false);
        QStringList temp;

        foreach(QString listitems, downloadList) {
            int hittat = listitems.indexOf(',', 0);
            temp.append(listitems.left(hittat));
        }

        bool folders = false;

        if(temp.removeDuplicates() > 0) {
            folders = true;
        }

        if(ui->actionCreateFolder->isChecked() || folders) {
            ui->teOut->clear();
        } else {
            ui->teOut->setText(tr("The video streams are saved in ") + "\"" +
                               QDir::toNativeSeparators(save_all_path) + "\"");
        }

        downloadList.removeDuplicates();
        // int i = 1;
        QString preferred, quality, provider, sub, folderaddress, stcookie;
        // filnummer = 0;
        antalnedladdade = downloadList.size();
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
        settings.setIniCodec("UTF-8");
        QString *save_all_path2 = new QString;

// loop
        for(int antal = 0; antal < downloadList.size(); antal++) {
            ARG << "--force";
            ARG << "--verbose";
            QStringList list = downloadList.at(antal).split(",");
            address = list.at(0);
            quality = list.at(1);
            preferred = list.at(2);
            provider = list.at(3);
            sub = list.at(4);
            stcookie = list.at(5);
            folderaddress = address;
            QString username, password;

            if(provider != "npassword") {
                QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                                   DISPLAY_NAME, EXECUTABLE_NAME);
                settings.setIniCodec("UTF-8");
                settings.beginGroup(QStringLiteral(u"Provider"));
                username = settings.value(provider + QStringLiteral(u"/username")).toString();
                password = settings.value(provider + QStringLiteral(u"/password")).toString();
                {
                    if(password == "") {
                        bool ok;
                        QInputDialog inputdialog;
                        inputdialog.setOkButtonText(tr("Ok"));
                        inputdialog.setCancelButtonText(tr("Cancel"));
                        inputdialog.setWindowTitle(tr("Enter your password"));
                        inputdialog.setInputMode(QInputDialog::TextInput);
                        inputdialog.setTextEchoMode(QLineEdit::Password);
                        inputdialog.setLabelText(tr("Spaces are not allowed. Use only the characters\nyour streaming provider approves."));
                        ok = inputdialog.exec();
                        QString newpassword = inputdialog.textValue();

                        if(newpassword.indexOf(' ') >= 0) {
                            return;
                        }

                        if(ok) {
                            secretpassword = newpassword;
                            password = newpassword;
                        } else {
                            return;
                        }
                    }
                }
            }

            //             Windows
            CommProcess2 = new QProcess(this);
            // SPARA VÄRDET PÅ "PATH" och konkatenera med det nya värdet
            QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
            const QString path =
                QDir::toNativeSeparators(QCoreApplication::applicationDirPath());
            const QString oldpath = env.value("PATH");
#ifdef Q_OS_LINUX
            env.insert("PATH", path + ":" + oldpath); // Add an environment variable
#endif
#ifdef Q_OS_WIN                                 // QT5
            env.insert("PATH", path + ";" + oldpath); // Add an environment variable
#endif
            CommProcess2->setProcessEnvironment(env);
            CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
            /* MAKE FOLDER */
            int hittat = folderaddress.lastIndexOf('/');
            folderaddress = folderaddress.mid(hittat);

            if(ui->actionCreateFolder->isChecked() || folders) {
                QDir dir(save_all_path + folderaddress + "/" + preferred + "_" + quality);

                if(!dir.exists()) {
                    dir.mkpath(".");
                }

                *save_all_path2 = dir.path();
                ui->teOut->append(tr("The video streams are saved in ") +
                                  QDir::toNativeSeparators(*save_all_path2) + "\"");
            } else {
                *save_all_path2 = save_all_path;
            }

            if(ui->actionCopyToDefaultLocation->isChecked()) {
                ui->teOut->append(kopierastill);
            }

            CommProcess2->setWorkingDirectory(
                QDir::toNativeSeparators(*save_all_path2));

            /* END MAKE FOLDER */

            if(stcookie != "nst") {
                ARG << "--cookies"  << stcookie;
            }

            if(provider != "npassword") {
                ARG << "--username" << username << "--password" << password;
            }

            if(sub == "ysub") {  // Subtitles
                ARG << "--subtitle";
            }

            if(preferred != "unknown" && quality != "unknown") {
                ARG << "--quality" << quality << "--preferred" << preferred << "--flexible-quality" << amount;
            }

            ARG << address;
            ui->teOut->append(tr("Preparing to download") + ": " + address);
            connect(CommProcess2,
                    QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            [ = ](int exitCode, QProcess::ExitStatus exitStatus) {
                statusExit(exitStatus, exitCode);
//                QString fil = *save_all_path2 + folderaddress + ".mp4";
                int lastslash = save_all_path2->lastIndexOf("/");
                QString folderpath = save_all_path2->mid(0, lastslash);
                QString fil = folderpath + "/" + preferred + "_" + quality + folderaddress + ".mp4";
                QString filnamn = folderaddress.mid(1);

                if(!avbrutet) {
                    if(QFile::exists(fil)) {
                        ui->teOut->setTextColor(QColor("darkBlue"));
                        ui->teOut->append(tr("Download ") + tr(" succeeded (") +  filnamn + ".mp4" + " (" + preferred + ", " + quality + ", " + amount + ")");
                        ui->progressBar->setValue(10);
                        int langd = fil.size();
                        QString path_undertextfilen =
                            fil.replace(langd - 3, 3, "srt");

                        if(QFile::exists(path_undertextfilen)) {
                            ui->teOut->append(tr("Download ") + tr(" succeeded (") + filnamn + ".srt" + " (" + preferred + ", " + quality + ", " + amount + ")");
                            ui->progressBar->setValue(10);
                        }

                        ui->teOut->setTextColor(QColor("black"));

                        if(ui->actionCopyToDefaultLocation->isChecked()) {
                            if(ui->actionCreateFolder->isChecked() || folders) {
                                QString tmp = preferred + "_" + quality;
                                copyToDefaultLocation(filnamn + ".mp4", &folderaddress,
                                                      &tmp);
                            } else {
                                copyToDefaultLocation(filnamn + ".mp4", &folderaddress);
                            }
                        }
                    } else {
                        ui->teOut->setTextColor(QColor("red"));
                        ui->teOut->append(tr("The download failed. If a username and password or 'st' cookie is required, you must enter these.") +  filnamn + ".mp4" + " (" + preferred + ", " + quality + ", " + amount + ")");
                        ui->teOut->setTextColor(QColor("black"));

                        if(!ui->actionNotifications->isChecked()) {
                            // showNotification
                            showNotification(tr("The download failed. If a username and password or 'st' cookie is required, you must enter these.") +  filnamn + ".mp4" + " (" + preferred + ", " + quality + ", " + amount + ")", 3);
                        }

                        avbrutet = false;
                    }
                }

                antalnedladdade--;

                if(antalnedladdade == 0) {
                    if(ui->actionCopyToDefaultLocation->isChecked()) {
                        ui->teOut->append(kopierastill);
                    }

                    ui->teOut->append(tr("Download completed"));
                    ui->progressBar->setValue(0);
                    ui->teOut->setTextColor(QColor("black"));

                    if(!ui->actionNotifications->isChecked()) {
                        // showNotification
                        showNotification(tr("Download completed"), 1);
                    }

                    processpid = 0;
                }

                /* ... */
            });
            connect(CommProcess2, SIGNAL(started()), this,
                    SLOT(onCommProcessStart()));
            // HIT
            ARG << "-o" << *save_all_path2 + folderaddress;
            CommProcess2->start(svtplaydl, ARG);
            ARG.clear();
            processpid = CommProcess2->processId();
            connect(CommProcess2, &QProcess::readyReadStandardOutput, [this]() {
                QString result(CommProcess2->readAllStandardOutput());

                if(ui->actionShowMore->isChecked()) {
                    ui->teOut->append(result.trimmed());

                    // progressbar
                    if(ui->progressBar->value() >= 100) {
                        ui->progressBar->setValue(0);
                    }

                    ui->progressBar->setValue(ui->progressBar->value() + 1);
                } else {
                    // progressbar
                    if(ui->progressBar->value() >= 100) {
                        ui->progressBar->setValue(0);
                    }

                    if(result.trimmed().contains("Merge audio and video")) {
                        ui->teOut->append(tr("Merge audio and video..."));
                    }

                    if(result.trimmed().contains("removing old files")) {
                        ui->teOut->append(tr("Removing old files, if there are any..."));
                    }

                    ui->progressBar->setValue(ui->progressBar->value() + 1);
                }
            });
        }
    }
}
