20:30 June 25, 2021 
Version 0.29.0 BETA 3
A major reworking of the program, it should work better to choose
download quality and all messages from the program
should be clear.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

17:00 June 25, 2021 
Version 0.29.0 BETA 2
A major reworking of the program, it should work better to choose
download quality and all messages from the program
should be clear.
Updated svtplay-dl latest snapshot 4.0-2-g09843d6
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

03:00 June 21, 2021 
Version 0.29.0 BETA 1
A major reworking of the program, it should work better to choose
download quality and all messages from the program
should be clear.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

20:00 June 20, 2021 
Version 0.28.15
Important bug fix: The download list now works.
Does not work earlier if you used the "Add to Downloads list" button.
The menu worked.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

15:00 June 20, 2021 
Version 0.28.14
Significantly improved decompression speed of downloaded
svtplay-dl.zip (Windows).
The Linux version can decompress *.tar.gz and *.zip
The Windows version can decompress *.tar.gz and *.zip
Improved dialogs for saving media files and selecting the
svtplay-dl download location.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

20:00 June 19, 2021 
Version 0.28.13 BETA 3
Significantly improved decompression speed of downloaded
svtplay-dl.zip (Windows).
The Linux version can decompress *.tar.gz and *.zip
The Windows version can decompress *.tar.gz and *.zip
Improved dialogs for saving media files and selecting the
svtplay-dl download location.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

17:35 June 18, 2021 
Version 0.28.12
Updated svtplay-dl stable version 4.0
Updated svtplay-dl latest snapshot 4.0  
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

23:30 June 15, 2021 
Version 0.28.11
"download svtplay-dl from bin.ceicer.com" shares the language
settings with the main application.
Minor improvements.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

19:00 June 14, 2021 
Version 0.28.10 BETA
"download svtplay-dl from bin.ceicer.com" shares the language
settings with the main application.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

23:59 June 13, 2021 
Version 0.28.9
Updated svtplay-dl latest snapshot 3.9.1-2-g00e5c84 
Maintenance Tool starts with "Update components" (Windows).
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

23:45 June 12, 2021
Version 0.28.8
Nicer message box when updating.
Moved the configuration file for "download svtplay-dl".
Visit source code and binary files.
Full Italian translation.
Updated Italian help file.
Minor improvements.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

10:30 June 10, 2021
Version 0.28.7 BETA
Nicer message box when updating.
Moved the configuration file for "download svtplay-dl".
Visit source code and binary files.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

00:30 June 10, 2021
Version 0.28.6
Bug fix: Unable to download. Removed "--remux"     
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

21:30 June 7, 2021
Version 0.28.5
Updated svtplay-dl stable version 3.9.1
Updated svtplay-dl latest snapshot 3.9.1      
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

18:30 June 4, 2021
Version 0.28.4
Updated svtplay-dl latest snapshot 3.8-27-gd32bc02        
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

22:14 June 3, 2021
Version 0.28.3
Updated svtplay-dl latest snapshot 3.8-25-g88432ed        
"-list quality" (search function) updated.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

19:30 May 30, 2021
Version 0.28.2
Updated svtplay-dl latest snapshot 3.8-23-g89284c9
Added "Auto Select" option for bitrate and method.           
"-list quality" (search function) further updated.
Opportunity to test language files in "svtplay-dl from bin.ceicer.com"
Full Italian translation.
Updated help files, English, Italian and Swedish.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

01:48 May 28, 2021
Version 0.28.0                   
"--list-quality" (search function) updated.
Works with the latest svtplay-dl.
New: Download svtplay-dl from streamCapture2.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

21:48 May 24, 2021
Version 0.27.6                    
Updated svtplay-dl latest snapshot 3.8-22-g45fceaa
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

22:48 May 23, 2021
Version 0.27.5                    
Updated svtplay-dl latest snapshot 3.8-18-g1c3d8f1
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

17:30 May 20, 2021
Version 0.27.4                    
Updated svtplay-dl latest snapshot 3.8-13-g3d46e85
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

19:30 May 17, 2021
Version 0.27.3
Complete Italian translation.
Updated svtplay-dl latest snapshot 3.8-10-gc9a606d
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

14:05 May 16, 2021
Version 0.27.2
'st' cookie works with the download list.
Updated svtplay-dl latest snapshot 3.8-3-g107d3e0
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

05:05 May 16, 2021
Version 0.27.1
Bug Fix: Failed to search for video streams when login required.
Manage many different 'st' cookies.
Edit the configuration file.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

17:55 May 14, 2021
Version 0.26.7
Updated svtplay-dl latest snapshot 3.8-2-g2c5101a
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

20:55 May 13, 2021
Version 0.26.6
Updated svtplay-dl stable version 3.8
Updated svtplay-dl latest snapshot 3.8
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

15:55 May 13, 2021
Version 0.26.5
Updated svtplay-dl latest snapshot 3.7-16-g494c9e3
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

20:55 May 11, 2021
Version 0.26.4
Updated svtplay-dl latest snapshot 3.7-12-ga5e4166
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

23:55 May 10, 2021
Version 0.26.3
Updated svtplay-dl latest snapshot 3.7-10-gd575112
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

11:05 May 9, 2021
Version 0.26.2
Updated svtplay-dl latest snapshot 3.7-4-g3497e05
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

14:05 May 6, 2021
Version 0.26.1
Possible to use 'st' cookies.
Optimized, more efficient code.
Updated svtplay-dl stable version 3.7
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

12:45 April 28, 2021
Version 0.25.8
Updated svtplay-dl latest snapshot 3.7
Improved display of downloadable video streams.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

21:00 April 22, 2021
Version 0.25.6
Bug fix: Unable to search for video streams.
Updated svtplay-dl latest snapshot 3.6-3-gaae55fc
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

19:00 April 20, 2021
Version 0.25.5
Updated svtplay-dl stable version 3.6
Updated svtplay-dl latest snapshot 3.6-2-g0dcb01f
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

23:00 April 15, 2021
Version 0.25.4
Updated svtplay-dl stable version 3.5
Updated svtplay-dl latest snapshot 3.5-1-g8805627
Updated FFmpeg 4.4
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

23:59 April 13, 2021
Version 0.25.3
Updated svtplay-dl stable version 3.4
Changes in the code to make the program a little faster and slightly smaller.
Possible to edit the name of streaming services.
Possible to have spaces in the name of streaming services.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

00:30 April 8, 2021
Version 0.25.1
Bug fixed: Error opening your own language file in the program.
Nicer presentation of "Method" and "Bitrate".
Help translated into Italian.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

12:31 April 3, 2021
Version 0.25.0
Updated svtplay-dl latest snapshot 3.3-2-gcdf394e
Name change of "Pay TV" to "Login"
"Direct Download of all Video Streams in current serie (Not from the Download List)" shows when each file has been downloaded and how many files remain.
Improved progress bar.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

08:31 March 17, 2021
Version 0.24.0
Updated svtplay-dl stable version 3.3
Updated svtplay-dl latest snapshot 3.3
Progress indicator showing the downloads.
Bug fix: Now you can see the number of searches you have previously chosen to save.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

10:00 March 16, 2021
Version 0.23.2
Updated svtplay-dl latest snapshot 3.2-10-g7440e3f
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

12:05 March 15, 2021
Version 0.23.1
Updated svtplay-dl latest snapshot 3.2-9-g028971b
Updated manual.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

03:30 March 13, 2021
Version 0.23.0
Bugfix download from Viafree.
Drag and drop function added.
More messages in the status bar.
Better translations in Linux AppImage.
Updated svtplay-dl stable version 3.2
Updated svtplay-dl latest snapshot 3.2-2-g6ef5d61.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

21:30 March 6, 2021
Version 0.22.4
Updated svtplay-dl latest snapshot 3.1-5-gcb3612b (Linux, Windows).
Linux AppImage updated to GLIBC 2.27 (2018-02-01). Now works with most distributions from May 2018 and newer.
Clearer messages if you try to copy to the default folder and the file already exists.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

00:30 March 1, 2021
Version 0.22.3
Updated svtplay-dl latest snapshot 3.1-4-g998f51f (Windows).
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux).

12:00 February 27, 2021
Version 0.22.2
"Update" is disabled if there is no update (Linux).
Minor corrections
Updated graphical interface.
Updated FFmpeg 4.3.2 (Linux)
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux).

00:00 February 26, 2021
Version 0.22.1
Updated svtplay-dl stable version 3.0-7-ge423c1c (Linux)
Updated svtplay-dl stable version 3.1 (Windows)
Updated svtplay-dl latest snapshot 3.1 (Linux, Windows)
Updated FFmpeg 4.3.2
Updated graphical interface.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux).

14:50 February 18, 2021
Version 0.22.0
Updated svtplay-dl latest snapshot 3.0-5-g8cd5793.
Italian translation.
Incomplete German translation removed.
Opportunity to test language files.
"About" shows a working e-mail address.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux).

00:50 February 15, 2021
Version 0.21.2
Updated svtplay-dl stable version 2.9-8-ga263a66
Updated svtplay-dl latest snapshot 3.0
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux).

12:50 January 25, 2021
Version 0.21.1
AppImage: "Delete all settings and Exit" 
Removes shortcuts on the desktop and in the application menu.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux).

22:50 January 24, 2021
Version 0.21.0
Updated svtplay-dl latest snapshot 2.8-11-gd193679.
AppImage: Desktop file and icon on the desktop.
AppImage: Desktop file and icon in the application menu.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux).

22:50 January 4, 2021
Version 0.20.18
Updated svtplay-dl latest snapshot 2.8-9-gc3ff052
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux). 

21:02 December 9, 2020
Version 0.20.17
Updated svtplay-dl stable version 2.8-3-g18a8758
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux). 

19:02 November 21, 2020
Version 0.20.16
Updated svtplay-dl stable version 2.8
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux). 

18:01 November 15, 2020
Version 0.20.15
Updated svtplay-dl version 2.7-5-gbf1f9c5
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux). 

21:55 November 6, 2020
Version 0.20.14
Updated svtplay-dl version 2.7-4-gef557cb
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux). 

03:45 November 1, 2020
Version 0.20.13
Warning for too high bitrate in case of failed download.
More reliable update of AppImage.
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux). 

18:34 October 27, 2020
Version 0.20.12
Bug fix: svtplay-dl in system path not found.
Updated svtplay-dl version 2.7
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).  

18:09 October 13, 2020
Version 0.20.11
Updated svtplay-dl version 2.7-3-ga550bd1
More reliable update of AppImage.
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).       

18:09 October 11, 2020
Version 0.20.10
Updated svtplay-dl version 2.6-4-gd6dc139
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).

20:09 October 2, 2020
Version 0.20.9
Updated svtplay-dl version 2.6
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).

22:05 September 27, 2020
Version 0.20.8
Updated svtplay-dl version 2.5-7-g7db9f7e
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).

19:39 September 23, 2020
Version 0.20.7
Updated svtplay-dl stable version 2.5
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).

19:39 September 22, 2020
Version 0.20.6
Updated svtplay-dl version 2.5-4-ge92ebbb
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).

00:55 September 20, 2020
Version 0.20.5
Updated svtplay-dl version 2.5
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).

00:43 September 19, 2020
Version 0.20.4
Updated svtplay-dl version 2.4-71-g5fc7750
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).

19:43 September 14, 2020
Version 0.20.3
Bug fix: Works better with the latest versions of svtplay-dl.
Updated svtplay-dl version 2.4-67-g478fd3b
Updated FFmpeg to version 4.3.1 (Linux)
Uses Qt 5.15.0, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.0, Compiled with GCC 5.4.0 (Linux).

18:43 September 13, 2020
Version 0.20.2
Opportunity to create a shortcuts.
The program updates itself. (Applies to Linux AppImage).
Updated svtplay-dl version 2.4-62-g2920bb9
Uses Qt 5.15.0, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.0, Compiled with GCC 5.4.0 (Linux).

15:20 July 12, 2020
Version 0.20.1
Opportunity to create a shortcut on the desktop.
Opportunity to receive notifications when download is complete.
Bug fix: The program could disappear if you switch between different number of screens.
Bug fix: Swedish translation of "FFmpeg can be found in system path" has been corrected.
Minor improvements.
Uses Qt 5.15.0, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.0, Compiled with GCC 5.4.0 (Linux).

13:26 June, 28 2020
Version 0.20.0
Opportunity to select svtplay-dl from the system path.
Opportunity to select svtplay-dl from the file system.
Bug fix: Now streamCapture remembers the selected font.
Minor improvements.
Uses Qt 5.15.0, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.0, Compiled with GCC 5.4.0 (Linux).

21:43 June, 15 2020
Version 0.19.2
Bug fix: Unpleasant bug that caused the program to crash.
This happened when the program was looking for updates at program startup.
Uses Qt 5.15.0, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.0, Compiled with GCC 5.4.0 (Linux).

17:07 June, 14 2020
Version 0.19.1
Uses the included FFmpeg or, if not included, the system's FFmpeg.
Uses Qt 5.15.0, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.0, Compiled with GCC 5.4.0 (Linux).

23:28 May, 3 2020
Version 0.19.0
Updated svtplay-dl version 2.4-42-g916e199
Opportunity to switch between svtplay-dl stable and latest snapshot.
Updated FFmpeg version 4.2.3
Installation packages for Linux and Windows. Shortcuts on the start menu.
Easy update with minimal download.
Uses Qt 5.15.0, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.0, Compiled with GCC 5.4.0 (Linux).

20:27 April, 27 2020
Version 0.18.5
Bug fix: Shortcuts on "Start", "Program menu" broken in version 0.18.4 (Windows).
Link to svtplay-dl forum for issues.
Selectable font.
Better contrasting colors between font and background.
Minor improvements.
Uses Qt 5.14.2, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.14.2, Compiled with GCC 5.4.0 (Linux).

12:42 April, 9 2020
Version 0.18.4
Updated font.
Updated swedish translation.
New background color.
Uses Qt 5.14.2, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.14.2, Compiled with GCC 5.4.0 (Linux).

01:09 April, 3 2020
Version 0.18.3
Updated svtplay-dl version 2.4-35-g81cb18f
Uses Qt 5.14.2, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.14.2, Compiled with GCC 5.4.0 (Linux).

13:49 February, 2 2020
Version 0.18.2
Updated svtplay-dl version 2.4-31-g25d3105
Updated FFmpeg version 4.2.2
Uses Qt 5.14.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.14.0, Compiled with GCC 5.4.0 (Linux).

12:35 December, 31 2019
Version 0.18.1
Check that svtplay-dl and ffmpeg are included and found.
No indication of svtplay-dl version in the "About" dialog.
Uses Qt 5.14.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.14.0, Compiled with GCC 5.4.0 (Linux).

16:23 December, 20 2019
Version 0.18.0
After "List all Episodes", select and download selected episodes.
Uses Qt 5.14.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.14.0, Compiled with GCC 5.4.0 (Linux).

22:55 November, 25 2019
Version 0.17.0
Improved display of search results.
Opportunity to use a preselected folder for download. 
Possibility to use a default folder where the media files and subtitles are copied. 
Ability to list all episodes. 
Ability to download all episodes at once. 
Opportunity to cancel ongoing download. (Windows)
Ability to delete all configuration files.
Improved subtitle management.
Many bug fixes.
Corrected misspellings
"Uninstaller" removed from "Portable" version.
Uses Qt 5.13.2, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.2, Compiled with GCC 5.4.0 (Linux).

11:01 October, 20 2019
Version 0.16.0
Static FFmpeg version 4.2.1 (Linux and Windows).
Updated svtplay-dl version 2.4-29-gc59a305
Clearer message from svtplay-dl about the search result.
Minor bug fixes.
Uses Qt 5.13.1, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, Compiled with GCC 5.4.0 (Linux).

21:49 October 11, 2019
Version 0.15.0
Bug: "/" at the end of the address is now deleted.
Menu selection to uninstall (Windows).
Updated svtplay-dl version 2.4-26-g5dcc899
Uses Qt 5.13.1, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, Compiled with GCC 5.4.0 (Linux).

21:24 September 16, 2019
Version 0.14.0
Removed question marks from search address.
Uses Qt 5.13.1, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, Compiled with GCC 5.4.0 (Linux).

19:53 September 16, 2019
Version 0.13.9
Updated svtplay-dl version 2.4-20-g0826b8f
Uses Qt 5.13.1, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, Compiled with GCC 5.4.0 (Linux).

23:24 September 13, 2019
Version 0.13.8
Updated svtplay-dl version 2.4-6-gce9bfd3
Uses Qt 5.13.1, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, Compiled with GCC 5.4.0 (Linux).

21:59 September  11, 2019
Version 0.13.7
Updated svtplay-dl version 2.4-2-g5466853
Uses Qt 5.13.1, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, Compiled with GCC 5.4.0 (Linux).

09:19 September  7, 2019
Version 0.13.6
Updated svtplay-dl version 2.3-23-gbc15c69
Updated ffmpeg version 4.2 (Windows).
Uses Qt 5.13.1, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, Compiled with GCC 5.4.0 (Linux).

22:17 September  2, 2019
Version 0.13.5
Updated svtplay-dl version 2.2-5-gd1904b2 (Linux).
Updated svtplay-dl version 2.2-7-g62cbf8a (Windows).
Clearer code and comments.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux).

00:06 September  1, 2019
Version 0.13.4
Updated svtplay-dl version 2.2-2-g838b3ec (Linux).
Updated svtplay-dl version 2.2-14-gc77a4a5 (Windows).
Minor bug. Don't show "Search..." after unsuccessful search.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux).

18:03  August  28, 2019
Version 0.13.3
Updated svtplay-dl version 2.2-1-g9146d0d
Download link points to gitlab.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux).

09:48  August  24, 2019
Version 0.13.2
Updated svtplay-dl version 2.1-65-gb64dbf3
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux).

17:43  August  21, 2019
Version 0.13.1
Bug fix: "Automatically checks for updates when the program starts" now works.
Updated svtplay-dl version 2.1-57-gf429cfc
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux).

17:59  August  17, 2019
Version 0.13.0
Information about what is updated when the program checks for updates.
Improved "About".
Moved "Password" from "File" menu to "Pay TV".
Better status messages.
Confirmation that the video stream has been downloaded.
Display numbers of downloaded streams.
Operating system dependent display of path.
Minor bug fix.
Architecture instruction set 64-bit.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux).

12:07  August  6, 2019
Version 0.12.0
Updated svtplay-dl (version 2.1-56-gbe6005e, Windows).
Downgraded ffmpeg (Windows, version 4.1.4, stable).
Create automatic folders, if needed. (Same video stream in different qualities).
Ability to view more information from svtplay-dl.
Add status-tip text.
Architecture instruction set 64-bit.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows)
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux)

16:32 July 31, 2019
Version 0.11.0
Architecture instruction set 64-bit.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows)
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux)

22:41 July 30, 2019
Version 0.11.0 RC 1
Improved help and manual.
Editable Download list.
Possibility to save the same stream in different sizes.
Ability to enter user name and password. For paid streaming services.
Replace obsolete code with more modern.
Use QSettings.
Several bug fixes.
Architecture instruction set 64-bit.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows)
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux)

14:39 July 5, 2019
Version 0.10.7
Several bug fixes. Works better to download a single file.
Duplicate in the selection of different video quality removed.
Architecture instruction set 64-bit.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows)
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux).

14:59 June 29, 2019
Version 0.10.5
Updated svtplay-dl version 2.1-53-gd33186e
Updated ffmpeg version N-94129-g098ab93257 (Windows).
Architecture instruction set 64-bit.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux).

21:29 February 13, 2018
Version 0.10.3
Updated svtplay-dl (version 1.9.7).
Architecture instruction set 64-bit.
Uses Qt 5.10.0, Compiled with MSVC++ 15.5 (MSVS 2017).

15:41 August 9, 2017
Version 0.10.1
Check box to search for, load and insert subtitles in the video file.
Dynamic user interface.
Updated ffmpeg (version 3.3.3).
Architecture instruction set 64-bit.
Uses Qt 5.9.1, Compiled with MSVC++ 15.0 (MSVS 2017).

01:19 July 26, 2017
Version 0.10.0
Combo Box to choose quality.
Architecture instruction set 64-bit.
Uses Qt 5.9.1, Compiled with MSVC++ 15.0 (MSVS 2017).

15:48 July 16, 2017
Version 0.9.4
Updated ffmpeg (version 3.3.2).
Architecture instruction set 64-bit.
Uses Qt 5.9.1, Compiled with MSVC++ 15.0 (MSVS 2017).

09:38 May 7, 2017
Version 0.9.3
Updated svtplay-dl (version 1.9.4).
Updated ffmpeg (version 3.2.4).
Display Version history

16:33 January 27, 2017
Version 0.9.2
Updated svtplay-dl (version 1.9.1).

18:00 January 14, 2017
Version 0.9.1
Updated svtplay-dl.
Updated ffmpeg.
German translation.
Minor improvments.

01:55 October 30, 2016
Release version 0.9.0
