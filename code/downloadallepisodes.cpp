//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "info.h"
#include "newprg.h"
#include "ui_newprg.h"
void Newprg::downloadAllEpisodes()
{
    if(!fileExists(svtplaydl)) {
        ui->teOut->setTextColor(QColor("red"));
        ui->teOut->setText(svtplaydl +
                           tr(" cannot be found or is not an executable program."));
        ui->teOut->append(tr("Please click on \"Tools\" and select svtplay-dl."));
        ui->teOut->setTextColor(QColor("black"));
        return;
    }

    QStringList ARG;
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup(QStringLiteral(u"Stcookies"));
    QString stcookie = settings.value(QStringLiteral(u"stcookie"), "").toString();
    settings.endGroup();
    ARG << "--verbose";
    ARG << "--force";
    ARG << "--all-episodes";

    if(ui->chbSubtitle->isChecked()) {
        ARG << "--subtitle";
    }

    if(!stcookie.isEmpty()) {
        ARG << "--cookies" << stcookie;
    }

    if(ui->actionCopyToDefaultLocation->isChecked()) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("You have chosen to copy to \"Default folder\".\nUnfortunately, "
                          "this does not work when you select \"Download all episodes\"."));
        msgBox.addButton(QMessageBox::Ok);
        msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
        msgBox.exec();
    }

    if(ui->actionCreateFolder->isChecked()) {
        QMessageBox msgBox;
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setIcon(QMessageBox::Information);
        msgBox.setText(tr("Because you cannot select \"Method\" and \"Quality\" when you "
                          "select \"Download all episodes\", no folders will be "
                          "created.\n\nDo you want to start the download?"));
        QAbstractButton *yesButton = msgBox.addButton(QMessageBox::Yes);
        msgBox.addButton(QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);
        msgBox.setButtonText(QMessageBox::Yes, tr("Yes"));
        msgBox.setButtonText(QMessageBox::No, tr("No"));
        msgBox.exec();

        if(msgBox.clickedButton() != yesButton) {
            return;
        }
    }

#ifdef Q_OS_LINUX
    QMessageBox msgBox;
    msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setText(tr("Once svtplay-dl has started downloading all episodes, streamCapture2 "
                      "no longer has control. "
                      "If you want to cancel, you may need to log out or restart your "
                      "computer.\nYou can try to cancel using the command\n\"sudo killall python3\"\n\nDo you want to start the download?"));
    QAbstractButton *yesButton = msgBox.addButton(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::Yes);
    msgBox.setButtonText(QMessageBox::Yes, tr("Yes"));
    msgBox.setButtonText(QMessageBox::No, tr("No"));
    msgBox.exec();

    if(msgBox.clickedButton() != yesButton) {
        return;
    }

#endif
    ui->teOut->setTextColor(QColor("black"));
    settings.beginGroup(QStringLiteral(u"Path"));
    QString defaultdownloadlocation =
        settings.value(QStringLiteral(u"defaultdownloadlocation")).toString();
    settings.endGroup();
    ui->teOut->setReadOnly(true);
    ui->actionSaveDownloadList->setEnabled(false);
    ui->teOut->setText(
        tr("The request is processed...\nPreparing to download..."));
    /*  */
    ui->actionSaveDownloadList->setEnabled(false);
    QString save_path;

    if(ui->actionDownloadToDefaultLocation->isChecked()) {
        /* */
        const QFileInfo downloadlocation(
            QDir::toNativeSeparators(defaultdownloadlocation));

        if(!downloadlocation.exists()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("The default folder for downloading video streams cannot be "
                              "found.\nDownload is interrupted."));
            msgBox.addButton(QMessageBox::Ok);
            msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
            msgBox.exec();
            ui->teOut->clear();
            return;
        }

        if(!downloadlocation.isWritable()) {
            QMessageBox::critical(this, DISPLAY_NAME " " VERSION,
                                  tr("You do not have the right to save to the "
                                     "folder.\nDownload is interrupted."));
            ui->teOut->clear();
            return;
        }

        /* */
        save_path = defaultdownloadlocation;
    } else {
        save_path = save();
    }

    if(save_path != QStringLiteral(u"nothing")) {
        /* END CREATE FOLDER */
        CommProcess2 = new QProcess(nullptr);
        // Linux
        // SPARA VÄRDET PÅ "PATH" och konkatenera med det nya värdet
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        const QString path =
            QDir::toNativeSeparators(QCoreApplication::applicationDirPath());
        const QString oldpath = env.value("PATH");
#ifdef Q_OS_LINUX
        env.insert("PATH", path + ":" + oldpath); // Add an environment variable
#endif
#ifdef Q_OS_WIN                               // QT5
        env.insert("PATH", path + ";" + oldpath); // Add an environment variable
#endif
        CommProcess2->setProcessEnvironment(env);
        //
        CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
        //
        CommProcess2->setWorkingDirectory(save_path);

        if(ui->comboPayTV->currentIndex() != 0)  { // Password
            /* */
            QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                               DISPLAY_NAME, EXECUTABLE_NAME);
            settings.setIniCodec("UTF-8");
            QString provider = ui->comboPayTV->currentText();
            settings.beginGroup(QStringLiteral(u"Provider"));
            QString username = settings.value(provider + QStringLiteral(u"/username")).toString();
            QString password = settings.value(provider + QStringLiteral(u"/password")).toString();

            if(password == "") {
                password = secretpassword;

                if(password == "") {
                    bool ok;
                    QString newpassword = QInputDialog::getText(
                                              nullptr, provider + " " + tr("Enter your password"),
                                              tr("Spaces are not allowed. Use only the characters your "
                                                 "streaming provider approves.\nThe Password will not be "
                                                 "saved."),
                                              QLineEdit::Password, "", &ok);

                    if(newpassword.indexOf(' ') >= 0) {
                        return;
                    }

                    if(ok) {
                        secretpassword = newpassword;
                    }
                }
            }

            ARG << "--username" << username << "--password" << password;
        }

        /* */
        ui->teOut->append(tr("Starts downloading all episodes: ") + address);
        connect(CommProcess2, SIGNAL(started()), this, SLOT(onCommProcessStart()));
        connect(CommProcess2, &QProcess::readyReadStandardOutput, [this]() {
            QString result(CommProcess2->readAllStandardOutput());

            if(ui->actionShowMore->isChecked()) {
                ui->teOut->append(result);

                // progressbar
                if(ui->progressBar->value() >= 100) {
                    ui->progressBar->setValue(0);
                }

                ui->progressBar->setValue(ui->progressBar->value() + 10);
            } else {
                // progressbar
                if(ui->progressBar->value() >= 100) {
                    ui->progressBar->setValue(0);
                }

                ui->progressBar->setValue(ui->progressBar->value() + 10);

                if(result.trimmed().contains("Merge audio and video")) {
                    ui->teOut->append(tr("Merge audio and video..."));
                } else {
                    avbrutet = true;
                }

                if(result.trimmed().contains("removing old files")) {
                    ui->teOut->append(tr("Removing old files, if there are any..."));
                }

                if(result.trimmed().contains("Episode")) {
                    QString s = result.trimmed();
                    int slash = s.indexOf("Episode");
                    s = s.mid(slash + 5);
                    QRegExp rx("(\\d+)");
                    QStringList list;
                    int pos = 0;

                    while((pos = rx.indexIn(s, pos)) != -1) {
                        list << rx.cap(1);
                        pos += rx.matchedLength();
                    }

                    ui->teOut->append(tr("Episode") + " " + list.at(0) + " " + tr("of") + " " + list.at(1) + "...");
                }
            }
        });
        connect(CommProcess2,
                QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
        [ = ](int exitCode, QProcess::ExitStatus exitStatus) {
            statusExit(exitStatus, exitCode);

            if(!avbrutet) {
                ui->teOut->setTextColor(QColor("darkBlue"));
                ui->teOut->append(
                    tr("The media files (and if you have selected the "
                       "subtitles) have been downloaded."));
                ui->teOut->setTextColor(QColor("black"));
                ui->progressBar->setValue(0);

                if(!ui->actionNotifications->isChecked()) {
                    // showNotification
                    showNotification(tr("Download completed"), 1);
                }
            } else {
                ui->teOut->append(tr("Download completed"));
                ui->progressBar->setValue(0);

                if(!ui->actionNotifications->isChecked()) {
                    // showNotification
                    showNotification(tr("Download completed"), 3);
                }

//                ui->teOut->setTextColor(QColor("red"));
//                ui->teOut->append(tr("The download failed. If a username and password or 'st' cookie is required, you must enter these."));
//                ui->teOut->setTextColor(QColor("black"));
//                if(!ui->actionNotifications->isChecked()) {
//                    // showNotification
//                    showNotification(tr("The download failed"), 3);
//                }
            }

            /* ... */
        });
        ARG << address;
        CommProcess2->start(svtplaydl, ARG);
        ARG.clear();
        processpid = CommProcess2->processId();
    } else {
        ui->teOut->setText(tr("No folder is selected"));
    }
}
