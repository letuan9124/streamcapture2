//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include "checkupdate.h"
#include "checkupdate_global.h"
#include "info.h"
#include "ui_newprg.h"

void Newprg::dragEnterEvent(QDragEnterEvent *ev)
{
    ev->accept();
}
void Newprg::dropEvent(QDropEvent *ev)
{
    QList<QUrl> urls = ev->mimeData()->urls();
    QUrl url(urls.at(0));

    if(url.isValid()) {
        QString urlstring = url.toString();

        if((urlstring.left(7) != "http://") && (urlstring.left(8) != "https://")) {
            return;
        }

        ui->leSok->setText(urlstring);
    }
}
