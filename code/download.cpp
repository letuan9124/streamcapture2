//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "checkupdate.h"
#include "info.h"
#include "newprg.h"
#include "ui_newprg.h"
#include <QMessageBox>
/* DOWNLOAD */
void Newprg::download()
{
    if(!fileExists(svtplaydl)) {
        ui->teOut->setTextColor(QColor("red"));
        ui->teOut->setText(svtplaydl +
                           tr(" cannot be found or is not an executable program."));
        ui->teOut->append(tr("Please click on \"Tools\" and select svtplay-dl."));
        ui->teOut->setTextColor(QColor("black"));
        return;
    }

    QString amount = ui->comboAmount->currentText();
// progress
    QStringList ARG;
    ARG << "--verbose";
    ARG << "--force";
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup(QStringLiteral(u"Stcookies"));
    QString stcookie = settings.value(QStringLiteral(u"stcookie"), "").toString();
    settings.endGroup();

    if(!stcookie.isEmpty()) {
        ARG << "--cookies"  << stcookie;
    }

    ui->teOut->setTextColor(QColor("black"));
    avbrutet = false;
    ui->teOut->setReadOnly(true);
    ui->actionSaveDownloadList->setEnabled(false);
    ui->teOut->setText(
        tr("The request is processed...\nPreparing to download..."));
    /*  */
    ui->actionSaveDownloadList->setEnabled(false);
    settings.beginGroup(QStringLiteral(u"Path"));
    QString defaultdownloadlocation =
        settings.value(QStringLiteral(u"defaultdownloadlocation")).toString();
    QString copypath = settings.value(QStringLiteral(u"copypath")).toString();
    settings.endGroup();
    QString save_path;
    QString save_path2;

    if(ui->actionDownloadToDefaultLocation->isChecked()) {
        save_path = QDir::toNativeSeparators(defaultdownloadlocation);
    } else {
        save_path = QDir::toNativeSeparators(save());
    }

    if(save_path != "nothing") {
        QString kopierastill;
        QString preferred = ui->leMethod->text();
//        preferred = preferred.toLower();
        QString quality = ui->leQuality->text();
        /* CREATE FOLDER */
        QString folderaddress = address;

        if(folderaddress.right(1) == '/') {
            int storlek = folderaddress.size();
            folderaddress.remove(storlek - 1, 1);
        }

        int hittat = folderaddress.lastIndexOf('/');
        int hittat2 = folderaddress.indexOf('?');
        folderaddress = folderaddress.mid(hittat, hittat2 - hittat);
        save_path2 = QLatin1String("");

        if(ui->actionCreateFolder->isChecked()) {
            QDir dir(save_path + folderaddress + "/" + preferred + "_" + quality);

            if(!dir.exists()) {
                dir.mkpath(".");
            }

            save_path2 = dir.path();
            ui->teOut->setText(tr("The video stream is saved in ") + "\"" +
                               QDir::toNativeSeparators(save_path2 + "\""));
        } else {
            save_path2 = save_path;
            ui->teOut->setText(tr("The video stream is saved in ") + "\"" +
                               QDir::toNativeSeparators(save_path + "\""));
        }

        if(ui->actionDownloadToDefaultLocation->isChecked()) {
            const QFileInfo downloadlocation(
                QDir::toNativeSeparators(defaultdownloadlocation));

            if(!downloadlocation.exists()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("The default folder for downloading video streams cannot be "
                                  "found.\nDownload is interrupted."));
                msgBox.addButton(QMessageBox::Ok);
                msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.exec();
                ui->teOut->clear();
                return;
            }

            if(!downloadlocation.isWritable()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("You do not have the right to save to the "
                                  "folder.\nDownload is interrupted."));
                msgBox.addButton(QMessageBox::Ok);
                msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.exec();
                ui->teOut->clear();
                return;
            }

            kopierastill = tr("Selected folder to copy to is ") + "\"" +
                           QDir::toNativeSeparators(copypath + "\"");
        }

        /* END CREATE FOLDER */
        CommProcess2 = new QProcess(nullptr);
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        const QString path =
            QDir::toNativeSeparators(QCoreApplication::applicationDirPath());
        const QString oldpath = env.value("PATH");
#ifdef Q_OS_LINUX
        env.insert("PATH", path + ":" + oldpath); // Add an environment variable
#endif
#ifdef Q_OS_WIN                               // QT5
        env.insert("PATH", path + ";" + oldpath); // Add an environment variable
#endif
        QProcess *CommProcess2 = new QProcess;
        CommProcess2->setProcessEnvironment(env);
        CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
        CommProcess2->setWorkingDirectory(QDir::toNativeSeparators(save_path2));

        if(ui->chbSubtitle->isChecked()) {
            ARG << "--subtitle";
        }

        if(ui->comboBox->currentText().at(0) != '-') {
            ARG << "--quality" << quality << "--preferred" << preferred << "--flexible-quality" << amount;
        }

        if(ui->comboPayTV->currentIndex() != 0) {  // Password
            {
                /* */
                QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                                   DISPLAY_NAME, EXECUTABLE_NAME);
                settings.setIniCodec("UTF-8");
                QString provider = ui->comboPayTV->currentText();
                settings.beginGroup(QStringLiteral(u"Provider"));
                QString username = settings.value(provider + QStringLiteral(u"/username")).toString();
                QString password = settings.value(provider + QStringLiteral(u"/password")).toString();

                if(password == "") {
                    password = secretpassword;

                    if(password == "") {
                        bool ok;
                        QInputDialog inputdialog;
                        inputdialog.setOkButtonText(tr("Ok"));
                        inputdialog.setCancelButtonText(tr("Cancel"));
                        inputdialog.setWindowTitle(tr("Enter your password"));
                        inputdialog.setInputMode(QInputDialog::TextInput);
                        inputdialog.setTextEchoMode(QLineEdit::Password);
                        inputdialog.setLabelText(tr("Spaces are not allowed. Use only the characters\nyour streaming provider approves."));
                        ok = inputdialog.exec();
                        QString newpassword = inputdialog.textValue();

                        if(newpassword.indexOf(' ') >= 0) {
                            return;
                        }

                        if(ok) {
                            secretpassword = newpassword;
                        }
                    }
                }

                /* */
                ARG << "--username" << username << "--password" << password;
            }
        }

        /* */
        auto *filnamn = new QString;
        ui->teOut->append(tr("Starts downloading: ") + address);
        connect(CommProcess2, SIGNAL(started()), this, SLOT(onCommProcessStart()));
        connect(CommProcess2, &QProcess::readyReadStandardOutput,
        [this, filnamn, CommProcess2]() {
            QString result(CommProcess2->readAllStandardOutput());

            if(result.contains(".mp4")) {
                int stopp = result.indexOf(".mp4") + 4;
                int start = result.lastIndexOf(" ", stopp) + 1;
                int langd = result.size();
                int storlek = langd - start - stopp;
                QString tmp = result.mid(start, storlek);
                tmp = tmp.trimmed();
                *filnamn = tmp;
            }

            if(ui->actionShowMore->isChecked()) {
                // progressbar
                if(ui->progressBar->value() >= 100) {
                    ui->progressBar->setValue(0);
                }

                ui->progressBar->setValue(ui->progressBar->value() + 1);
                ui->teOut->append(result.trimmed());
            } else {
                // progressbar
                if(ui->progressBar->value() >= 100) {
                    ui->progressBar->setValue(0);
                }

                if(result.trimmed().contains("Merge audio and video")) {
                    ui->teOut->append(tr("Merge audio and video..."));
                }

                if(result.trimmed().contains("removing old files")) {
                    ui->teOut->append(tr("Removing old files, if there are any..."));
                }

                ui->progressBar->setValue(ui->progressBar->value() + 1);
            }
        });
        qInfo() << "preferred 1 " << preferred;
//        QString *newPreferred = &preferred;
        connect(
            CommProcess2,
            QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
        [ &, filnamn, preferred, quality, amount, kopierastill, folderaddress, save_path2 ](int exitCode, QProcess::ExitStatus exitStatus) {
            qInfo() << "preferred " << preferred;

            if(!avbrutet) {
                if(filnamn->isEmpty()) {
                    ui->teOut->setTextColor(QColor("red"));
                    QString x = *filnamn;
                    x.replace("\\\\", "\\", Qt::CaseSensitivity::CaseInsensitive);
                    ui->teOut->append(tr("The download failed. Did you forget to enter username and password? Try another bitrate, or \"Auto select\". It often helps.") + " " + x + " (" + preferred + ", " + quality + ", " + amount + ")");

                    if(!ui->actionNotifications->isChecked()) {
                        showNotification(tr("The download failed. Did you forget to enter username and password? Try another bitrate, or \"Auto select\". It often helps.") + " " + x + " (" + preferred + ", " + quality + ", " + amount + ")", 3);
                    }

                    ui->teOut->setTextColor(QColor("black"));
                } else {
                    ui->teOut->setTextColor(QColor("darkBlue"));
                    QString x = *filnamn;
                    x.replace("\\\\", "\\", Qt::CaseSensitivity::CaseInsensitive);
                    ui->teOut->append(tr("Download succeeded ") + " " + x + " (" + preferred + ", " + quality + ", " + amount + ")");
                    ui->progressBar->setValue(10);
                    QString undertexten = *filnamn;
                    int langd = undertexten.size();
                    undertexten = undertexten.replace(langd - 3, 3, "srt");

                    if(QFile::exists(
                                QDir::toNativeSeparators(save_path2 + "/" + undertexten))) {
                        ui->teOut->append(tr("Download succeeded ") + "(" + undertexten +
                                          ")");
                    }

                    ui->teOut->setTextColor(QColor("black"));

                    if(ui->actionCopyToDefaultLocation->isChecked()) {
                        copyToDefaultLocation(*filnamn, &folderaddress);
                    }

                    ui->teOut->append(tr("Download completed"));
                    ui->progressBar->setValue(0);

                    if(!ui->actionNotifications->isChecked()) {
                        // showNotification
                        showNotification(tr("Download completed"), 1);
                    }

                    ui->teOut->setTextColor(QColor("black"));

                    if(ui->actionCopyToDefaultLocation->isChecked()) {
                        ui->teOut->append(kopierastill);
                    }

                    // hit
                }
            } else {
                ui->teOut->setTextColor(QColor("red"));
                ui->teOut->append(tr("The download failed "));
                ui->teOut->setTextColor(QColor("black"));

                if(!ui->actionNotifications->isChecked()) {
                    // showNotification
                    showNotification(tr("The download failed "), 3);
                }

                avbrutet = false;
            }

            statusExit(exitStatus, exitCode);
            /* ... */
        });
        ARG << address;
        CommProcess2->setProgram(svtplaydl);
        CommProcess2->setArguments(ARG);
        CommProcess2->waitForFinished();
        CommProcess2->start();
    } else {
        ui->teOut->setText(tr("No folder is selected"));
    }
}

/* END DOWNLOAD */
void Newprg::initSok()
{
    if(!ui->actionSvtplayDlSystem->isChecked()) {
        if(!fileExists(svtplaydl)) {
            return;
        }
    }

    ui->teOut->setTextColor("black");
    ui->teOut->setReadOnly(true);
    ui->actionSaveDownloadList->setEnabled(false);
    ui->comboBox->clear();
    ui->leQuality->setText(tr("Searching..."));
    ui->leMethod->setText(tr("Searching..."));
    ui->teOut->setText(tr("The request is processed...\nStarting search..."));
}
