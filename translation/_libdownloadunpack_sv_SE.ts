<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>DownloadUnpack</name>
    <message>
        <location filename="downloadunpack.cpp" line="36"/>
        <location filename="downloadunpack.cpp" line="36"/>
        <source>svtplay-dl from bin.ceicer.com</source>
        <translation>svtplay-dl från bin.ceicer.com</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="64"/>
        <location filename="downloadunpack.cpp" line="64"/>
        <source>Uses</source>
        <translation>Använder</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="104"/>
        <location filename="downloadunpack.cpp" line="104"/>
        <source>Is not writable. Cancels</source>
        <translation>Är inte skrivbar. Avbryter</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="133"/>
        <source>to</source>
        <translation>till</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="135"/>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="135"/>
        <source>Check your file permissions and antivirus software.</source>
        <translation>Kontrollera dina filbehörigheter och antivirusprogram.</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="133"/>
        <location filename="downloadunpack.cpp" line="133"/>
        <source>Can not save</source>
        <translation>Kan inte spara</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="135"/>
        <location filename="downloadunpack.cpp" line="135"/>
        <source>Can not save to</source>
        <translation>Kan inte spara till</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="138"/>
        <location filename="downloadunpack.cpp" line="138"/>
        <source>is downloaded to</source>
        <translation>är nedladdat till</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="153"/>
        <location filename="downloadunpack.cpp" line="153"/>
        <source>Starting to decompress</source>
        <translation>Börjar dekomprimera</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="170"/>
        <location filename="downloadunpack.cpp" line="170"/>
        <source>is decompressed to</source>
        <translation>är dekomprimerad till</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="173"/>
        <location filename="downloadunpack.cpp" line="205"/>
        <location filename="downloadunpack.cpp" line="173"/>
        <location filename="downloadunpack.cpp" line="205"/>
        <source>Failed to decompress.</source>
        <translation>Misslyckades med att dekomprimera.</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="239"/>
        <location filename="downloadunpack.cpp" line="239"/>
        <source>files</source>
        <translation>filer</translation>
    </message>
    <message>
        <location filename="downloadunpack.cpp" line="241"/>
        <location filename="downloadunpack.cpp" line="241"/>
        <source>file</source>
        <translation>fil</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="19"/>
        <location filename="downloadunpack.ui" line="19"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="157"/>
        <location filename="downloadunpack.ui" line="218"/>
        <location filename="downloadunpack.ui" line="157"/>
        <location filename="downloadunpack.ui" line="218"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="46"/>
        <location filename="downloadunpack.ui" line="206"/>
        <location filename="downloadunpack.ui" line="46"/>
        <location filename="downloadunpack.ui" line="206"/>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="176"/>
        <location filename="downloadunpack.ui" line="176"/>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="184"/>
        <location filename="downloadunpack.ui" line="184"/>
        <source>&amp;Tools</source>
        <translation>&amp;Verktyg</translation>
    </message>
    <message>
        <source>&amp;Language</source>
        <translation>&amp;Språk</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="190"/>
        <location filename="downloadunpack.ui" line="190"/>
        <source>&amp;Visit</source>
        <translation>&amp;Besök</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="226"/>
        <location filename="downloadunpack.ui" line="226"/>
        <source>Try to decompress</source>
        <translation>Försöker att dekomprimera</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="275"/>
        <location filename="downloadunpack.ui" line="275"/>
        <source>Source code...</source>
        <translation>Källkod...</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="283"/>
        <location filename="downloadunpack.ui" line="283"/>
        <source>Binary files...</source>
        <translation>Binära filer...</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="115"/>
        <location filename="downloadunpack.ui" line="235"/>
        <location filename="downloadunpack.ui" line="115"/>
        <location filename="downloadunpack.ui" line="235"/>
        <source>Exit</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="238"/>
        <location filename="downloadunpack.ui" line="238"/>
        <source>F4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="247"/>
        <location filename="downloadunpack.ui" line="247"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="256"/>
        <location filename="downloadunpack.ui" line="256"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="261"/>
        <location filename="downloadunpack.ui" line="261"/>
        <source>Load external language file...</source>
        <translation>Ladda extern språkfil...</translation>
    </message>
    <message>
        <location filename="downloadunpack.ui" line="270"/>
        <location filename="downloadunpack.ui" line="270"/>
        <source>Italian</source>
        <translation>Italienska</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="filedialog.cpp" line="35"/>
        <location filename="filedialog.cpp" line="35"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <source>Download svtplay-dl to</source>
        <translation>Ladda ner svtplay-dl till</translation>
    </message>
    <message>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Programmet måste startas om för att de nya språkinställningarna ska träda i kraft.</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation>Starta om</translation>
    </message>
    <message>
        <source>Open your language file</source>
        <translation>Öppna din språkfil</translation>
    </message>
    <message>
        <source>Compiled language file (*.qm)</source>
        <translation>Kompilerad språkfil (*.qm)</translation>
    </message>
    <message>
        <location filename="filedialog.cpp" line="34"/>
        <location filename="filedialog.cpp" line="34"/>
        <source>Choose</source>
        <translation>Välj</translation>
    </message>
    <message>
        <location filename="filedialog.cpp" line="36"/>
        <location filename="filedialog.cpp" line="36"/>
        <source>Save svtplay-dl in directory</source>
        <translation>Spara svtplay-dl i folder</translation>
    </message>
</context>
</TS>
