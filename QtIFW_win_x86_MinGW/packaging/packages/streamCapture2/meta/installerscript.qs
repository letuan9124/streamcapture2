

function Component()
{
   gui.pageWidgetByObjectName("LicenseAgreementPage").entered.connect(changeLicenseLabels);
}

Component.prototype.createOperations = function()
{
    component.createOperations();
	


    if (systemInfo.productType === "windows") {
        component.addOperation("CreateShortcut", "@TargetDir@/streamcapture2.exe", "@StartMenuDir@/streamCapture2.lnk",
            "workingDirectory=@TargetDir@", "iconPath=@TargetDir@/streamcapture2.ico");

			
		//component.addOperation("CreateShortcut", "@TargetDir@/streamcapture2.exe", "@DesktopDir@/streamCapture2.lnk",
        //    "workingDirectory=@TargetDir@", "iconPath=@TargetDir@/streamcapture2.ico");

    }
    
         if (installer.value("os") === "x11") {
     
                            component.addOperation("CreateDesktopEntry",
                            "streamcapture2.desktop",
                            "Version=1.0\nType=Application\nExec=@TargetDir@/AppRun %F\nName=streamCapture2\nComment=Download video streams\nIcon=@TargetDir@/streamcapture2.png\nX-GNOME-Gettext-Domain=@TargetDir@/AppRun\nStartupNotify=true\nCategories=AudioVideo;\n");

    }
}



changeLicenseLabels = function()
{
    page = gui.pageWidgetByObjectName("LicenseAgreementPage");
    page.AcceptLicenseLabel.setText("<font size=4>Yes I do!</font>");
    page.RejectLicenseLabel.setText("<font size=4>No I don't!</font>");
}
