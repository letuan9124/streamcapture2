//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include <QApplication>
#include <QLocale>
#include <QTranslator>

#include "checkupdate.h"
#include "info.h"
#include "newprg.h"
#include "ui_newprg.h"
#include <string>
int main(int argc, char *argv[])
{
    QString currentLocale(QLocale::system().name());
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString sp = settings.value("language", currentLocale).toString();
    settings.endGroup();
    QApplication app(argc, argv);
//    qInfo() <<  app.style();
//    app.setStyle(QStyleFactory::create("Fusion"));
//    app.setStyle(new QProxyStyle());
    /*   */
    QLocale locale(sp);
    const QString qttranslationsPath(":/i18n/qttranslations");
    QTranslator qtTranslator;

    if(qtTranslator.load(locale, "qt", "_", qttranslationsPath)) {
        QApplication::installTranslator(&qtTranslator);
    }

    QTranslator qtBaseTranslator;

    if(qtBaseTranslator.load(locale, "qtbase", "_", qttranslationsPath)) {
        QApplication::installTranslator(&qtBaseTranslator);
    }

    /*  */
    QString fontPath = ":/fonts/Ubuntu-R.ttf";
    int fontId = QFontDatabase::addApplicationFont(fontPath);

    if(fontId != -1) {
        QFont f("Ubuntu", FONT);
        QApplication::setFont(f);
    }

//        qDebug() << "BRA";
//    }
//    } else {
//        qDebug() << "KASS";
//    }
    QTranslator translator;
//    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
//                       EXECUTABLE_NAME);
//    settings.setIniCodec("UTF-8");
// Test translation
    settings.beginGroup("Language");
    QString testlanguage = settings.value("testlanguage").toString();
    QString language = settings.value("language", "").toString();
    settings.endGroup();
    QFileInfo fi(testlanguage);

    if((language.isEmpty()) && (fi.exists())) {
        if(translator.load(testlanguage)) {
            QApplication::installTranslator(&translator);
//            settings.beginGroup("Language");
//            settings.setValue("testlanguage", "");
//            settings.endGroup();
        }
    }
// END Test translation
    else {
        // ELSE
        settings.beginGroup("Language");
        QString sp = settings.value("language", "").toString();
        settings.endGroup();
        const QString translationPath = ":/i18n/_complete";
//        const QString built_inTranslation = ":/i18n";

        if(sp == "") {
            sp = QLocale::system().name();

            if(translator.load(translationPath + "_" + sp + ".qm")) {
                settings.beginGroup("Language");
                settings.setValue("language", sp);
                settings.endGroup();
                /*  */
                /*  */
                QApplication::installTranslator(&translator);
            } else if(sp != "en_US") {
                QMessageBox::information(
                    nullptr, DISPLAY_NAME " " VERSION,
                    DISPLAY_NAME
                    " is unfortunately not yet translated into your language, the "
                    "program will start with English menus.");
            }
        } else {
            if(translator.load(translationPath + "_" + sp + ".qm")) {
                QApplication::installTranslator(&translator);
            }
        }

        // END ELSE Test translation
    }

    Newprg mNewPrg;
// End config
    QObject::connect(&app, &QCoreApplication::aboutToQuit,
                     [&mNewPrg]() -> void { mNewPrg.setEndConfig(); });
    mNewPrg.show();
    int r = QApplication::exec();
    /* END CONFIG */
    /* settings.beginGroup("MainWindow");
     settings.setValue("size", w.size());
     settings.setValue("pos", w.pos());
     settings.endGroup();*/
//    QApplication::removeTranslator(&qtTranslator);
//    QApplication::removeTranslator(&qtBaseTranslator);
//    QApplication::removeTranslator(&translator);
    return r;
}
