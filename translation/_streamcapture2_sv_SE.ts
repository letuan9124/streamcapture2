<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>Info</name>
    <message>
        <location filename="../info.cpp" line="40"/>
        <source> streamCapture2 handles downloads of video streams.&lt;br&gt;</source>
        <translation> streamCapture2 hanterar nedladdningar av videoströmmar.&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="41"/>
        <source>Many thanks to </source>
        <translation>Många tack till </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="35"/>
        <source>A graphical shell for </source>
        <translation>Ett grafiskt skal för </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="37"/>
        <source> and </source>
        <translation> och </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="32"/>
        <source> is free software, license </source>
        <translation> är fri mjukvara, licens </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="42"/>
        <source> for the Italian translation.</source>
        <translation> för den italienska översättningen.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="63"/>
        <source> was created </source>
        <translation> skapades </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="64"/>
        <source>by a computer with</source>
        <translation>av en dator med</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="111"/>
        <source>Full version number </source>
        <translation>Fullständigt versionsnummer </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="193"/>
        <source>Unknown version</source>
        <translation>Okänd version</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="204"/>
        <source>Unknown compiler.</source>
        <translation>Okänd kompilator.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="231"/>
        <source>Phone: </source>
        <translation>Telefon: </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="245"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="224"/>
        <source>Home page</source>
        <translation>Hemsida</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="25"/>
        <source>bovirus</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="78"/>
        <location filename="../info.cpp" line="96"/>
        <location filename="../info.cpp" line="197"/>
        <source>Compiled by</source>
        <translation>Kompilerad med</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="219"/>
        <source>Revision</source>
        <translation>Revision</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="226"/>
        <source>Source code</source>
        <translation>Källkod</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="228"/>
        <source>Wiki</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="242"/>
        <source>This program uses Qt version </source>
        <translation>Programmet använder Qt version </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="243"/>
        <source> running on </source>
        <translation> och körs på </translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../newprg.cpp" line="592"/>
        <location filename="../newprg.cpp" line="616"/>
        <source>Enter your password</source>
        <translation>Skriv in ditt lösenord</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="593"/>
        <location filename="../newprg.cpp" line="617"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation>Det är inte tillåtet med mellanslag. Använd endast skrivtecken som din streaminleverantör godkänner.
Lösenordet sparas inte.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="682"/>
        <source>svtplay-dl crashed.</source>
        <translation>svtplay-dl kraschade.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="688"/>
        <source>Could not stop svtplay-dl.</source>
        <translation>Kunde inte stoppa svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="699"/>
        <source>Delete any files that may have already been downloaded.</source>
        <translation>Ta bort alla filer som kan ha blivit nedladdade.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="696"/>
        <source>svtplay-dl stopped. Exit code </source>
        <translation>svtplay-dl stoppade. Exit kod </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="721"/>
        <source>Copy to: </source>
        <translation>Kopiera till: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="806"/>
        <source>Download to: </source>
        <translation>Ladda ner till: </translation>
    </message>
</context>
<context>
    <name>Newprg</name>
    <message>
        <location filename="../about.cpp" line="65"/>
        <source>The version history file is not found.</source>
        <translation>Kan inte hitta filen med versionshistoriken.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="85"/>
        <location filename="../about.cpp" line="111"/>
        <location filename="../about.cpp" line="135"/>
        <source>The license file is not found.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="157"/>
        <source> could not be found. Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install </source>
        <translation> kan inte hittas. Välj &quot;Verktyg&quot;, &quot;Underhållsverktyg&quot; för att installera </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="160"/>
        <source>Or install </source>
        <translation>Eller installera </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="160"/>
        <source> in your system.</source>
        <translation> i ditt system.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="199"/>
        <location filename="../newprg.cpp" line="118"/>
        <source>svtplay-dl is not found in the system path.</source>
        <translation>svtplay-dl finns inte i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="201"/>
        <location filename="../newprg.cpp" line="120"/>
        <source>You can use svtplay-dl that comes with streamCapture2.</source>
        <translation>Du kan använda svtplay-dl som kommer med streamCapture2.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="204"/>
        <location filename="../newprg.cpp" line="85"/>
        <source>svtplay-dl.exe is not found in the system path.</source>
        <translation>svtplay-dl.exe finns inte i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="206"/>
        <location filename="../newprg.cpp" line="87"/>
        <source>You can use svtplay-dl.exe that comes with streamCapture2.</source>
        <translation>Du kan använda svtplay-dl.exe som kommer med streamCapture2.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="217"/>
        <source> cannot be found. Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install.</source>
        <translation> kan inte hittas. Gå till &quot;Verktyg&quot;, &quot;Underhållsverktyg&quot; för att installera.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="223"/>
        <source> cannot be found.</source>
        <translation> kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="228"/>
        <source>svtplay-dl cannot be found or is not an executable program. Please select svtplay-dl manually.</source>
        <translation>svtplay-dl kan inte hittas eller är inte ett körbart program. Vänligen välj svvtplay-dl manuellt.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="233"/>
        <source>svtplay-dl.exe cannot be found or is not an executable program. Please select svtplay-dl.exe manually.</source>
        <translation>svtplay-dl.exe kan inte hittas eller är inte ett körbart program. Vänligen välj svtplay-dl.exe manuellt.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="243"/>
        <source>version </source>
        <translation>version </translation>
    </message>
    <message>
        <location filename="../language.cpp" line="36"/>
        <location filename="../language.cpp" line="75"/>
        <location filename="../language.cpp" line="114"/>
        <source>Restart Now</source>
        <translation>Starta om nu</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="39"/>
        <location filename="../language.cpp" line="78"/>
        <location filename="../language.cpp" line="117"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Programmet måste startas om för att de nya språkinställningarna ska börja gälla.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="185"/>
        <location filename="../downloadall.cpp" line="174"/>
        <location filename="../language.cpp" line="35"/>
        <location filename="../language.cpp" line="74"/>
        <location filename="../language.cpp" line="113"/>
        <location filename="../newprg.cpp" line="741"/>
        <location filename="../newprg.cpp" line="768"/>
        <location filename="../newprg.cpp" line="977"/>
        <location filename="../newprg.cpp" line="1453"/>
        <location filename="../newprg.cpp" line="1515"/>
        <location filename="../paytv_create.cpp" line="61"/>
        <location filename="../paytv_create.cpp" line="90"/>
        <location filename="../paytv_create.cpp" line="122"/>
        <location filename="../paytv_edit.cpp" line="33"/>
        <location filename="../paytv_edit.cpp" line="114"/>
        <location filename="../paytv_edit.cpp" line="145"/>
        <location filename="../paytv_edit.cpp" line="202"/>
        <location filename="../save.cpp" line="36"/>
        <location filename="../st_create.cpp" line="29"/>
        <location filename="../st_create.cpp" line="37"/>
        <location filename="../st_edit.cpp" line="33"/>
        <location filename="../st_edit.cpp" line="110"/>
        <location filename="../st_edit.cpp" line="124"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="243"/>
        <source>Up and running</source>
        <oldsource>Upp and running</oldsource>
        <translation>Uppe och kör</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="41"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="42"/>
        <source>Update this AppImage to the latest version</source>
        <translation>Uppdatera den här AppImagen till senaste version</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="373"/>
        <source>FFmpeg cannot be found or is not an executable program. Please reinstall the program.
Or install FFmpeg in the system path.</source>
        <translation>FFmpeg kan inte hittas eller är inte ett körbart program. Installera om programmet.
Eller installera FFmpeg i systemvägen.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="382"/>
        <source>FFmpeg cannot be found or is not an executable program. Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install or download portable again.
Or install FFmpeg in the system path.</source>
        <translation>FFmpeg kan inte hittas eller är inte ett körbart program. Gå till &quot;Verktyg&quot;, &quot;Underhållsverktyg&quot; för att installera eller ladda ner AppImage igen.
Eller installera FFmpeg i systemets sökväg.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="472"/>
        <source>Could not save a file to store Recent Search list. Check your file permissions.</source>
        <oldsource>Could not save a file to store Recent Searche list. Check your file permissions.</oldsource>
        <translation>Kunde inte spara listan med tidigare.sökningar. Kolla dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="494"/>
        <source>Could not save a file to store the list off downloads. Check your file permissions.</source>
        <translation>Kunde inte spara listan med tidigare.nedladdningar. Kolla dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="57"/>
        <location filename="../downloadallepisodes.cpp" line="110"/>
        <source>The request is processed...
Preparing to download...</source>
        <translation>Begäran behandlas ...
Förbereder sig för att ladda ner ...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="138"/>
        <location filename="../downloadall.cpp" line="109"/>
        <source>Selected folder to copy to is </source>
        <translation>Vald folder att kopiera till är </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="350"/>
        <location filename="../listallepisodes.cpp" line="54"/>
        <source>The request is processed...
Starting search...</source>
        <translation>Begäran behandlas ...
Startar att söka...</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="69"/>
        <location filename="../sok.cpp" line="70"/>
        <source>The search field is empty!</source>
        <translation>Sökrutan är tom!</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="73"/>
        <location filename="../sok.cpp" line="74"/>
        <source>Incorrect URL</source>
        <translation>Felaktig URL</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1369"/>
        <source>Can not find any video streams, please check the address.

</source>
        <translation>Det går inte att hitta några videoströmmar , kontrollera adressen.

</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1325"/>
        <source>Downloading...</source>
        <translation>Laddar ner...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="210"/>
        <source>Starts downloading: </source>
        <translation>Startar nedladdningen: </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="245"/>
        <location filename="../downloadall.cpp" line="356"/>
        <location filename="../downloadallepisodes.cpp" line="233"/>
        <source>Removing old files, if there are any...</source>
        <translation>Ta bort gamla filer, om det finns några...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="331"/>
        <location filename="../downloadallepisodes.cpp" line="295"/>
        <source>No folder is selected</source>
        <translation>Ingen mapp är vald</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="185"/>
        <location filename="../listallepisodes.cpp" line="109"/>
        <location filename="../sok.cpp" line="114"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation>Det är inte tillåtet med mellanslag. Använd endast skrivtecken som din streaminleverantör godkänner.
Lösenordet sparas inte.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="135"/>
        <location filename="../downloadall.cpp" line="223"/>
        <source>The video streams are saved in </source>
        <oldsource>The video files are saved in </oldsource>
        <translation>Videoströmmarna sparas i </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="31"/>
        <location filename="../downloadall.cpp" line="30"/>
        <location filename="../downloadallepisodes.cpp" line="28"/>
        <location filename="../listallepisodes.cpp" line="28"/>
        <location filename="../sok.cpp" line="34"/>
        <source> cannot be found or is not an executable program.</source>
        <translation> kan inte hittas eller är inget körbart program.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="32"/>
        <location filename="../downloadall.cpp" line="31"/>
        <location filename="../downloadallepisodes.cpp" line="29"/>
        <location filename="../listallepisodes.cpp" line="29"/>
        <location filename="../sok.cpp" line="35"/>
        <source>Please click on &quot;Tools&quot; and select svtplay-dl.</source>
        <translation>Vänligen klicka på &quot;Verktyg&quot; och välj svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="100"/>
        <location filename="../download.cpp" line="104"/>
        <source>The video stream is saved in </source>
        <oldsource>The video file is saved in </oldsource>
        <translation>Videoströmmen sparas i </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="116"/>
        <location filename="../downloadall.cpp" line="58"/>
        <location filename="../downloadallepisodes.cpp" line="124"/>
        <source>The default folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation>Standardmappen för att ladda ner videoströmmar kan inte hittas.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="129"/>
        <location filename="../downloadall.cpp" line="71"/>
        <location filename="../downloadall.cpp" line="104"/>
        <location filename="../downloadallepisodes.cpp" line="135"/>
        <source>You do not have the right to save to the folder.
Download is interrupted.</source>
        <translation>Du har inte rätten att spara i mappen du valt.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="241"/>
        <location filename="../downloadall.cpp" line="352"/>
        <location filename="../downloadallepisodes.cpp" line="227"/>
        <source>Merge audio and video...</source>
        <translation>Slår samman ljud och video...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="264"/>
        <location filename="../download.cpp" line="267"/>
        <source>The download failed. Did you forget to enter username and password? Try another bitrate, or &quot;Auto select&quot;. It often helps.</source>
        <translation>Nedladdningen misslyckades. Glömde du att ange användarnamn och lösenord? Försök med en annan bithastighet eller &quot;Auto select&quot;. Det hjälper ofta.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="311"/>
        <location filename="../download.cpp" line="316"/>
        <source>The download failed </source>
        <translation>Nedladdningen misslyckades </translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="95"/>
        <source>The default folder for copying video streams cannot be found.
Download is interrupted.</source>
        <translation>Standardmappen för att kopiera videoströmmar kan inte hittas.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="293"/>
        <location filename="../download.cpp" line="298"/>
        <location filename="../downloadall.cpp" line="312"/>
        <location filename="../downloadall.cpp" line="318"/>
        <location filename="../downloadallepisodes.cpp" line="268"/>
        <location filename="../downloadallepisodes.cpp" line="271"/>
        <location filename="../downloadallepisodes.cpp" line="276"/>
        <source>Download completed</source>
        <translation>Nedladdningen klar</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="56"/>
        <source>You have chosen to copy to &quot;Default folder&quot;.
Unfortunately, this does not work when you select &quot;Download all episodes&quot;.</source>
        <translation>Du har valt att kopiera till &quot;Förvald folder&quot;.
 Detta fungerar tyvärr inte när du väljer &quot;Ladda ner alla episoder&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="67"/>
        <source>Because you cannot select &quot;Method&quot; and &quot;Quality&quot; when you select &quot;Download all episodes&quot;, no folders will be created.

Do you want to start the download?</source>
        <translation>Eftersom du inte kan välja &quot;Metod&quot; och &quot;Kvalitet&quot; när du väljer &quot;Ladda ner alla avsnitt&quot;, skapas inga mappar.

Vill du starta nedladdningen?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="86"/>
        <source>Once svtplay-dl has started downloading all episodes, streamCapture2 no longer has control. If you want to cancel, you may need to log out or restart your computer.
You can try to cancel using the command
&quot;sudo killall python3&quot;

Do you want to start the download?</source>
        <translation>När svtplay-dl startat nedladdningarna av alla episoder har streamCapture2 inte längre någon kontroll. Om du vill avbryta kan du behöva logga ut eller starta om datorn.
Du kan försöka avbryta med kommandot
&quot;sudo killall python3&quot;

Vill du starta nedladdningen?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="204"/>
        <source>Starts downloading all episodes: </source>
        <translation>Börjar ladda ner alla avsnitt: </translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="249"/>
        <source>Episode</source>
        <translation>Avsnitt</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="249"/>
        <source>of</source>
        <translation>av</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="261"/>
        <source>The media files (and if you have selected the subtitles) have been downloaded.</source>
        <translation>Mediefilerna (och om du har valt undertexterna) har laddats ner.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="255"/>
        <source>Preparing to download</source>
        <translation>Förbereder att ladda ner</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="269"/>
        <location filename="../downloadall.cpp" line="276"/>
        <source> succeeded (</source>
        <translation> lyckades (</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="269"/>
        <location filename="../downloadall.cpp" line="276"/>
        <source>Download </source>
        <translation>Nedladdning </translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="293"/>
        <location filename="../downloadall.cpp" line="298"/>
        <source>The download failed. If a username and password or &apos;st&apos; cookie is required, you must enter these.</source>
        <translation>Nedladdningen misslyckades. Om ett användarnamn och lösenord eller &apos;st&apos; cookie krävs måste du ange dessa.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="275"/>
        <location filename="../download.cpp" line="283"/>
        <source>Download succeeded </source>
        <translation>Nedladdningen lyckades </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="348"/>
        <location filename="../download.cpp" line="349"/>
        <location filename="../listallepisodes.cpp" line="52"/>
        <location filename="../listallepisodes.cpp" line="53"/>
        <source>Searching...</source>
        <translation>Söker...</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="147"/>
        <location filename="../sok.cpp" line="150"/>
        <source> crashed.</source>
        <translation> kraschade.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="149"/>
        <location filename="../sok.cpp" line="152"/>
        <source>Can not find any video streams, please check the address.</source>
        <translation>Kan inte hitta någon videoström, kontrollera adressen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="148"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="151"/>
        <source>bold and italic</source>
        <translation>fet och kursiv</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="154"/>
        <source>bold</source>
        <translation>fet</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="156"/>
        <source>italic</source>
        <translation>kursivt</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="160"/>
        <source>Current font:</source>
        <translation>Nuvarande teckensnitt:</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="161"/>
        <source>size:</source>
        <translation>storlek:</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="292"/>
        <location filename="../setgetconfig.cpp" line="207"/>
        <source>Select &quot;Tools&quot;, &quot;Maintenance Tool&quot; and &quot;Update components&quot;.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="740"/>
        <location filename="../newprg.cpp" line="767"/>
        <location filename="../newprg.cpp" line="976"/>
        <location filename="../save.cpp" line="35"/>
        <source>Choose</source>
        <translation>Välj</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="769"/>
        <source>Download streaming media to directory</source>
        <translation>Ladda ner strömmande media till folder</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="924"/>
        <location filename="../newprg.cpp" line="933"/>
        <source>Cannot find </source>
        <translation>Kan inte hitta </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="924"/>
        <location filename="../newprg.cpp" line="933"/>
        <source> in system path</source>
        <translation> i systemsökvägen</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="978"/>
        <location filename="../newprg.cpp" line="996"/>
        <source>Select svtplay-dl</source>
        <translation>Välj svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="996"/>
        <source>*.exe (svtplay-dl.exe)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1012"/>
        <source>svtplay-dl is not an executable program.</source>
        <translation>svtplay-dl är inget körbart program.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1320"/>
        <location filename="../newprg.cpp" line="1321"/>
        <location filename="../newprg.cpp" line="1400"/>
        <source>Auto select</source>
        <translation>Välj automatiskt</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1391"/>
        <source>The search is complete</source>
        <translation>Sökningen är klar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1393"/>
        <source>The search failed</source>
        <translation>Sökningen misslyckades</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1509"/>
        <source>All your saved settings will be deleted. All lists of files to download will disappear. Do you want to continue?</source>
        <translation>Alla dina sparade inställningar raderas. Alla listor över filer som ska laddas ned försvinner. Vill du fortsätta?</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="367"/>
        <source>Path to svtplay-dl: </source>
        <translation>Sökväg till svtplay-dl: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="371"/>
        <source>Path to svtplay-dl.exe: </source>
        <translation>Sökväg till svtplay-dl.exe: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="845"/>
        <location filename="../newprg.cpp" line="890"/>
        <source>svtplay-dl cannot be found or is not an executable program. Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install or download AppImage again.</source>
        <translation>svtplay-dl kan inte hittas eller är inget körbart program. Gå till &quot;Verktyg&quot;, &quot;Underhållsverktyg&quot; för att installera eller ladda ner AppImage igen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="856"/>
        <source>svtplay-dl.exe cannot be found or is not an executable program. Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install or download portable again.</source>
        <translation>svtplay-dl.exe kan inte hittas eller är inget körbart program. Gå till &quot;Verktyg&quot;, &quot;Underhållsverktyg&quot; för att installera eller ladda ner portable igen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="342"/>
        <source>svtplay-dl is in the system path.</source>
        <translation>svtplay-dl finns i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="347"/>
        <source>svtplay-dl.exe is in the system path.</source>
        <translation>svtplay-dl.exe finns i systemsökvägenvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="353"/>
        <source>ERROR! svtplay-dl is not in the system path.</source>
        <translation>FEL! svtplay-dl finns inte i systemsökvägenvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="357"/>
        <source>ERROR! svtplay-dl.exe is not in the system path.</source>
        <translation>FEL! svtplay-dl.exe finns inte i systemsökvägenvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="378"/>
        <source>ERROR! svtplay-dl could not be found.</source>
        <translation>FEL! svtplay-dl kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="382"/>
        <source>ERROR! svtplay-dl.exe could not be found.</source>
        <translation>FEL! svtplay-dl.exe kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="398"/>
        <source>ERROR! FFmpeg cannot be found.</source>
        <translation>FEL! FFmpeg kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="401"/>
        <source>Path to FFmpeg: </source>
        <translation>Sökväg till FFmpeg: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="406"/>
        <source>FFmpeg can be found in the system path</source>
        <translation>FFmpeg finns i systemsökvägen</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="742"/>
        <location filename="../save.cpp" line="37"/>
        <source>Copy streaming media to directory</source>
        <translation>Kopiera mediafilen till folder</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1041"/>
        <source> cannot be found or is not an executable program. Click &quot;Tools&quot;, &quot;Select svtplay-dl...&quot;  to select svtplay-dl.</source>
        <translation> kan inte hittas eller är inte ett körbart program. Klicka &quot;Verktyg&quot;, &quot;Välj svtplay-dl...&quot; för att välja svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1108"/>
        <source> is it possible to update and uninstall the program.</source>
        <translation> är det möjligt att uppdatera och avinstallera programmet.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1106"/>
        <source>Maintenance Tool cannot be found.
Only if you install </source>
        <translation>Det går inte att hittta Underhållsverktyget.
Bara om du installerar </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1159"/>
        <source>Failed to create shortcut.
Check your file permissions.</source>
        <translation>Misslyckades med att skapa genvägar.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1140"/>
        <source>Failed to create desktop shortcut.
Check your file permissions.</source>
        <translation>Misslyckades med att skapa en genväg på skrivbordet.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../st_create.cpp" line="38"/>
        <location filename="../st_edit.cpp" line="125"/>
        <source>Enter &apos;st&apos; cookie</source>
        <translation>Ange &apos;st&apos; cookie</translation>
    </message>
    <message>
        <location filename="../st_create.cpp" line="40"/>
        <location filename="../st_edit.cpp" line="128"/>
        <source>Enter the &apos;st&apos; cookie that your video stream provider has saved in your browser.</source>
        <translation>Ange &apos;st&apos;-cookien som vidioströmleverantören har sparat i din webbläsare.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="287"/>
        <location filename="../setgetconfig.cpp" line="202"/>
        <source>Select &quot;Tools&quot;, &quot;Update&quot; to update.</source>
        <translation>Välj &quot;Verktyg&quot;, &quot;Uppdatera&quot; för att uppdatera.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="295"/>
        <location filename="../setgetconfig.cpp" line="210"/>
        <source>Download a new</source>
        <translation>Ladda ner en ny</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="901"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot;
to install or download portable again.</source>
        <translation>svtplay-dl kan inte hittas eller är inte ett körbart program.
Gå till &quot;Verktyg&quot;, &quot;Underhållsverktyg&quot;
för att installera eller ladda ner en portabel version.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1420"/>
        <source>Click to copy to the search box.</source>
        <translation>Klicka för att kopiera till sökrutan.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1443"/>
        <source>The number of previous searches to be saved...</source>
        <oldsource>The number of recently opened files to be displayed...</oldsource>
        <translation>Antalet tidigare sökningar som ska sparas...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1445"/>
        <source>Specify how many previous searches you want to save. If the number of searches exceeds the specified number, the oldest search is deleted.</source>
        <translation>Bestäm antalet tidigare sökningar som ska sparas. Om antalet sökningar överstiger det angivna antalet kommer den äldsta sökningen att tas bort.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1470"/>
        <source>Remove all saved searches</source>
        <translation>Ta bort alla tidigare sökningar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1473"/>
        <source>Click to delete all saved searches.</source>
        <translation>Klicka för att ta bort alla sparade sökningar.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1563"/>
        <source>Failed to delete your configuration files. Check your file permissions.</source>
        <translation>Misslyckades med att ta bort dina inställningsfiler. Kolla dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1578"/>
        <source> was normally terminated. QProsess Exit code = </source>
        <translation> avslutades normalt QProcess Exit kod = </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1583"/>
        <source> crashed. QProsess Exit code = </source>
        <translation> kraschade QProcess Exit code = </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1460"/>
        <source>The number of searches to be saved: </source>
        <oldsource>Set number of Recent Files: </oldsource>
        <translation>Ange maxantalet sparade sökningar: </translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="91"/>
        <location filename="../paytv_edit.cpp" line="116"/>
        <source>Enter your username</source>
        <translation>Skriv in ditt användarnamn</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="186"/>
        <location filename="../downloadall.cpp" line="175"/>
        <location filename="../downloadallepisodes.cpp" line="184"/>
        <location filename="../listallepisodes.cpp" line="108"/>
        <location filename="../paytv_create.cpp" line="123"/>
        <location filename="../paytv_edit.cpp" line="148"/>
        <location filename="../sok.cpp" line="113"/>
        <source>Enter your password</source>
        <translation>Skriv in ditt lösenord</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="67"/>
        <location filename="../about.cpp" line="87"/>
        <location filename="../about.cpp" line="113"/>
        <location filename="../about.cpp" line="137"/>
        <location filename="../coppytodefaultlocation.cpp" line="66"/>
        <location filename="../coppytodefaultlocation.cpp" line="167"/>
        <location filename="../download.cpp" line="119"/>
        <location filename="../download.cpp" line="132"/>
        <location filename="../download.cpp" line="184"/>
        <location filename="../downloadall.cpp" line="61"/>
        <location filename="../downloadall.cpp" line="74"/>
        <location filename="../downloadall.cpp" line="98"/>
        <location filename="../downloadall.cpp" line="173"/>
        <location filename="../downloadallepisodes.cpp" line="59"/>
        <location filename="../downloadallepisodes.cpp" line="127"/>
        <location filename="../newprg.cpp" line="849"/>
        <location filename="../newprg.cpp" line="858"/>
        <location filename="../newprg.cpp" line="894"/>
        <location filename="../newprg.cpp" line="903"/>
        <location filename="../newprg.cpp" line="926"/>
        <location filename="../newprg.cpp" line="935"/>
        <location filename="../newprg.cpp" line="1014"/>
        <location filename="../newprg.cpp" line="1045"/>
        <location filename="../newprg.cpp" line="1110"/>
        <location filename="../newprg.cpp" line="1452"/>
        <location filename="../newprg.cpp" line="1566"/>
        <location filename="../paytv_create.cpp" line="60"/>
        <location filename="../paytv_create.cpp" line="89"/>
        <location filename="../paytv_create.cpp" line="121"/>
        <location filename="../paytv_edit.cpp" line="113"/>
        <location filename="../paytv_edit.cpp" line="144"/>
        <location filename="../paytv_edit.cpp" line="201"/>
        <location filename="../setgetconfig.cpp" line="375"/>
        <location filename="../setgetconfig.cpp" line="386"/>
        <location filename="../setgetconfig.cpp" line="475"/>
        <location filename="../shortcuts.cpp" line="72"/>
        <location filename="../shortcuts.cpp" line="147"/>
        <location filename="../st_create.cpp" line="28"/>
        <location filename="../st_create.cpp" line="36"/>
        <location filename="../st_edit.cpp" line="109"/>
        <location filename="../st_edit.cpp" line="123"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="189"/>
        <location filename="../downloadall.cpp" line="178"/>
        <location filename="../paytv_create.cpp" line="93"/>
        <location filename="../paytv_create.cpp" line="126"/>
        <location filename="../paytv_edit.cpp" line="118"/>
        <location filename="../paytv_edit.cpp" line="150"/>
        <source>Spaces are not allowed. Use only the characters
your streaming provider approves.</source>
        <translation>Mellanslag är inte tillåtna. Använd endast tecken
som din leverantör godkänner.</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="62"/>
        <location filename="../paytv_edit.cpp" line="203"/>
        <location filename="../st_create.cpp" line="30"/>
        <location filename="../st_edit.cpp" line="111"/>
        <source>Streaming service</source>
        <translation>Strömningstjänst</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="64"/>
        <location filename="../paytv_edit.cpp" line="205"/>
        <location filename="../st_create.cpp" line="32"/>
        <location filename="../st_edit.cpp" line="114"/>
        <source>Enter the name of your streaming service.</source>
        <translation>Ange namnet på din strömningstjänst.</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="135"/>
        <location filename="../paytv_edit.cpp" line="160"/>
        <source>Save password?</source>
        <translation>Spara lösenordet?</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="137"/>
        <location filename="../paytv_edit.cpp" line="162"/>
        <source>Do you want to save the password? (unsafe)?</source>
        <translation>Vill du spara lösenordet (osäkert)?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="73"/>
        <location filename="../downloadallepisodes.cpp" line="93"/>
        <location filename="../newprg.cpp" line="1514"/>
        <location filename="../paytv_create.cpp" line="141"/>
        <location filename="../paytv_edit.cpp" line="166"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="74"/>
        <location filename="../downloadallepisodes.cpp" line="94"/>
        <location filename="../paytv_create.cpp" line="142"/>
        <location filename="../paytv_edit.cpp" line="167"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="28"/>
        <source>Manage Login details for </source>
        <translation>Hantera inloggningsuppgifter för </translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="29"/>
        <source>Edit, rename or delete
</source>
        <translation>Redigera, byt namn eller ta bort
</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="30"/>
        <location filename="../st_edit.cpp" line="31"/>
        <source>Edit</source>
        <translation>Redigera</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="31"/>
        <source>Rename</source>
        <translation>Byt namn</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="32"/>
        <location filename="../st_edit.cpp" line="32"/>
        <source>Delete</source>
        <oldsource>Delet</oldsource>
        <translation>Ta bort</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="59"/>
        <source>Create New</source>
        <translation>Skapa ny</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="67"/>
        <location filename="../setgetconfig.cpp" line="315"/>
        <source>No Password</source>
        <translation>Inget lösenord</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="64"/>
        <location filename="../coppytodefaultlocation.cpp" line="164"/>
        <source>You have not selected any place to copy the media files.
Please select a location before proceeding.</source>
        <translation>Du har inte valt någon plats att kopiera mediafilerna till.
Var vänlig och bestäm en plats innan du fortsätter.</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="84"/>
        <location filename="../coppytodefaultlocation.cpp" line="180"/>
        <source>A file with the same name already exists. The file will not be copied.</source>
        <translation>Det finns redan en fil med samma namn. Filen kopieras inte.</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="91"/>
        <location filename="../coppytodefaultlocation.cpp" line="115"/>
        <location filename="../coppytodefaultlocation.cpp" line="187"/>
        <location filename="../coppytodefaultlocation.cpp" line="207"/>
        <source>Copy succeeded </source>
        <translation>Kopieringen lyckades </translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="95"/>
        <location filename="../coppytodefaultlocation.cpp" line="119"/>
        <location filename="../coppytodefaultlocation.cpp" line="191"/>
        <location filename="../coppytodefaultlocation.cpp" line="211"/>
        <source>Copy failed </source>
        <translation>Kopieringen misslyckades </translation>
    </message>
    <message>
        <location filename="../qsystemtrayicon.cpp" line="27"/>
        <source>The mission failed!</source>
        <translation>Uppdraget misslyckades!</translation>
    </message>
    <message>
        <location filename="../qsystemtrayicon.cpp" line="30"/>
        <source>Mission accomplished!</source>
        <translation>Uppdrag slutfört!</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="70"/>
        <source>Failure!
The shortcut could not be created in
&quot;~/.local/share/applications&quot;
Check your file permissions.</source>
        <translation>Fel!
Genvägen kunde inte skapas i
&quot;~/.local/share/applikationer&quot;
Kontrollera dina filbehörigheter.</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="118"/>
        <location filename="../shortcuts.cpp" line="120"/>
        <source>Download video streams.</source>
        <translation>Ladda ner videoströmmar.</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="145"/>
        <source>Failure!
The shortcut could not be created.
Check your file permissions.</source>
        <translation>Fel!
Genvägen kunde inte skapas.
Kontrollera dina filbehörigheter.</translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="40"/>
        <source>Compiled language file (*.qm)</source>
        <translation>Kompilerade språkfil (*.qm)</translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="40"/>
        <source>Open your language file</source>
        <translation>Öppna din språkfil</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="26"/>
        <source>Manage &apos;st&apos; cookie for </source>
        <translation>Hantera &apos;st&apos; cookie för </translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="28"/>
        <source>Use, Do not use, Edit or Delete
&apos;st&apos; cookie for
</source>
        <translation>Använd, Använd inte, Redigera eller Ta bort
&apos;st&apos; cookie för
</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="29"/>
        <source>Use</source>
        <translation>Använd</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="30"/>
        <source>Do not use</source>
        <translation>Använd inte</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="175"/>
        <source>Set new &apos;st&apos; cookie</source>
        <translation>Ställ in en ny &apos;st&apos;-cookie</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="176"/>
        <source>Paste and save the &apos;st&apos; cookie that your streaming provider has downloaded to your browser.</source>
        <translation>Klistra in och spara &apos;st&apos; cookien som din streamingleverantör har laddat ner till din webbläsare.</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="42"/>
        <source>Quality, Method, Codec, Resolution, Language and Role</source>
        <translation>Kvalitet, Metod, Codec, Upplösning, Språk och Roll</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="191"/>
        <source>ERROR: No videos found. Cant find video id for the video.</source>
        <translation>FEL: Inga videor hittades. Kan inte hitta video-id för videon.</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="41"/>
        <source>The information from svtplay-dl may or may not contain:</source>
        <translation>Informationen kan innehålla, eller inte innehålla:</translation>
    </message>
    <message>
        <location filename="../zoom.cpp" line="33"/>
        <source>The font size changes to the selected font size</source>
        <translation>Teckenstorleken ändras till vald teckenstorlek</translation>
    </message>
</context>
<context>
    <name>newprg</name>
    <message>
        <location filename="../newprg.ui" line="114"/>
        <location filename="../newprg.ui" line="844"/>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="302"/>
        <source>Method</source>
        <translation>Metod</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="437"/>
        <location filename="../newprg.ui" line="871"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="622"/>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="32"/>
        <source>TEST</source>
        <translation>TEST</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="62"/>
        <source>Paste the link to the page where the video is displayed</source>
        <translation>Klistra in länken till sidan där videon visas</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="233"/>
        <source>Quality (Bitrate)</source>
        <oldsource>Bitrate</oldsource>
        <translation>Kvalitet (Bithastighet)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="287"/>
        <source>Media streaming communications protocol.</source>
        <translation>Protokoll för att strömma media.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="434"/>
        <source>Download the file you just searched for</source>
        <translation>Ladda ner filen som du just sökt efter</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="396"/>
        <location filename="../newprg.ui" line="1036"/>
        <source>Download all</source>
        <translation>Ladda ner alla</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="450"/>
        <source>Select quality on the video you download</source>
        <translation>Välj kvalitet på videon du laddar ner</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="146"/>
        <location filename="../newprg.ui" line="1021"/>
        <source>Add to Download list</source>
        <translation>Lägg till i nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="65"/>
        <location filename="../newprg.ui" line="856"/>
        <source>Paste</source>
        <translation>Klistra in</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="459"/>
        <source>Quality (bitrate) and method. Higher bitrate gives better quality and larger file.</source>
        <translation>Kvalitet (bithastighet) och metod. Högre bithastighet ger högre kvalitet och större fil.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="358"/>
        <location filename="../newprg.ui" line="998"/>
        <source>Include Subtitle</source>
        <translation>Inkludera undertexten</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="393"/>
        <source>Download all files you added to the list.</source>
        <translation>Ladda ner alla filer som du lagt till på listan.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="495"/>
        <source>Select quality on the video you download.</source>
        <translation>Välj kvalitet på videoströmmen du laddar ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="526"/>
        <source>Select provider. If yoy need a password.</source>
        <translation>Välj en tjänsteleverantör. Om du behöver lösenord.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="542"/>
        <location filename="../newprg.ui" line="983"/>
        <location filename="../newprg.ui" line="1103"/>
        <source>Password</source>
        <translation>Lösenord</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="634"/>
        <source>&amp;Language</source>
        <translation>&amp;Språk</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="643"/>
        <source>&amp;Tools</source>
        <translation>&amp;Verktyg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="677"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjälp</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="693"/>
        <source>&amp;Recent</source>
        <oldsource>Recent</oldsource>
        <translation>S&amp;enaste</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="698"/>
        <source>&amp;Download list</source>
        <translation>&amp;Nedladdningslista</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="720"/>
        <source>&amp;Several episodes</source>
        <translation>&amp;Flera avsnitt</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="729"/>
        <source>&apos;&amp;st&apos; cookies</source>
        <translation>&apos;&amp;st&apos; cookies</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="734"/>
        <source>View</source>
        <translation>Visa</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="758"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="767"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="775"/>
        <source>Check for updates at program start</source>
        <translation>Sök efter uppdateringar när programmet startar</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="778"/>
        <source>Check for software updates each time the program starts.</source>
        <oldsource>Check for software updates each time the program starts</oldsource>
        <translation>Kolla efter uppdateringar varje gång programmet startar.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="811"/>
        <source>Exit</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="814"/>
        <source>Exits the program.</source>
        <translation>Avsluta programmet.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="817"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="883"/>
        <source>License streamCapture2...</source>
        <translation>Licens streamCapture2...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="953"/>
        <source>Italian</source>
        <translation>Italienska</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="971"/>
        <source>Create new user</source>
        <translation>Skapa ny användare</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1047"/>
        <source>Create folder &quot;method_quality&quot;</source>
        <translation>Skapa folder &quot;metod_kvalitet&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1122"/>
        <source>Direct Download of all Video Streams in current serie (Not from the Download List)</source>
        <translation>Direktnedladdning av alla videoströmmar i nuvarande serie (inte från nedladdningslistan)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1125"/>
        <source>Searches for all Video Streams in the current series and tries to download them directly.</source>
        <translation>Söker efter alla videoströmmar i aktuell serie och försöker ladda ner dem direkt.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1147"/>
        <source>List all Video Streams</source>
        <translation>Lista alla videoströmmar</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1150"/>
        <source>Looking for Video Streams in the current series.</source>
        <translation>Letar efter videoströmmar i aktuell serie.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1166"/>
        <source>Copy to Selected Location</source>
        <translation>Kopiera till vald plats</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1291"/>
        <source>Use svtplay-dl from the system path</source>
        <translation>Använd svtplay-dl från systemsökvägen</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1311"/>
        <source>Use the selected svtplay-dl</source>
        <translation>Använd vald svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1319"/>
        <source>Do not show notifications</source>
        <translation>Visa inte aviseringar</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1322"/>
        <source>Do not show notifications when the download is complete.</source>
        <translation>Visa inte aviseringar när nedladdningen är klar.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1334"/>
        <source>Create a shortcut</source>
        <translation>Skapa en genväg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1345"/>
        <source>Desktop Shortcut</source>
        <translation>Skrivbordsgenväg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1348"/>
        <source>Create a shortcut to streamCapture2 on the desktop.</source>
        <translation>Skapa genväg på skrivbordet till streamCapture2.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1359"/>
        <source>Create a shortcut to streamCapture2 in the operating system menu.</source>
        <translation>Skapa genväg till streamCapture2 i operativsystemets meny.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1369"/>
        <source>Set new &apos;st&apos; cookie</source>
        <translation>Ange ny &apos;st&apos; cookie</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1374"/>
        <source>Edit settings (advanced)</source>
        <translation>Redigera inställningarna (avancerat)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1377"/>
        <source>Open the configuration file with the system text editor.</source>
        <translation>Öppna konfigurationsfilen med systemets text editor.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1386"/>
        <source>Download svtplay-dl...</source>
        <translation>Ladda ner svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1389"/>
        <source>Download and decompress svtplay-dl from bin.ceicer.com</source>
        <translation>Ladda ner och dekomprimera svtplay-dl från bin.ceicer.com</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1394"/>
        <source>Zoom In</source>
        <translation>Zooma in</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1397"/>
        <source>Increase the font size.</source>
        <translation>Öka teckensnittsstorleken.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1400"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1405"/>
        <source>Zoom Default</source>
        <translation>Zooma som standard</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1408"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1413"/>
        <source>Zoom Out</source>
        <translation>Zooma ut</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1416"/>
        <source>Decrease the font size.</source>
        <translation>Minska teckensnittsstorleken.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1419"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1356"/>
        <source>Applications menu Shortcut</source>
        <translation>Genväg i programmenyn</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1364"/>
        <source>Load external language file...</source>
        <translation>Ladda extern språkfil...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1184"/>
        <source>Save the location where the finished video file is copied.</source>
        <translation>Spara platsen dit den färdiga videofilen kopieras.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1193"/>
        <source>Select Default Download Location...</source>
        <translation>Välj standard nedladdningsplats...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1196"/>
        <source>Save the location for direct download.</source>
        <translation>Spara platsen för direkt nedladdning.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1207"/>
        <source>Direct download to the default location.</source>
        <translation>Direktnedladdning till standardplatsen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1215"/>
        <source>Add all Video Streams to Download List</source>
        <translation>Lägg till alla videoströmmar i nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1218"/>
        <source>Searches for all Video Streams in the current series and adds them to the download list.</source>
        <translation>Söker efter alla videoströmmar i aktuell serie och lägger till dem i nedladdningslistan.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1227"/>
        <source>Select font...</source>
        <translation>Välj teckensnitt...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1236"/>
        <source>Visit svtplay-dl forum for issues...</source>
        <translation>Besök svtplay-dl forum...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1244"/>
        <source>Maintenance Tool...</source>
        <translation>Underhållsverktyg...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1266"/>
        <source>Use the latest svtplay-dl snapshot</source>
        <translation>Använd senaste svtplay-dl snapshot</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1280"/>
        <source>Use latest stable svtplay-dl</source>
        <translation>Använd senaste stabila svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1283"/>
        <source>Latest Stable Release.</source>
        <translation>Senaste stabila versionen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1294"/>
        <source>Uses (if available) svtplay-dl in the system path.</source>
        <translation>Använder (om den finns) svtplay-dl i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1303"/>
        <source>Select svtplay-dl...</source>
        <translation>Välj svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1247"/>
        <source>Starts the Maintenance Tool. To update or uninstall.</source>
        <translation>Startar Underhållsverktyget. För att uppdatera eller avinstallera.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1181"/>
        <source>Select Copy Location...</source>
        <translation>Välj kopieringsplats...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1258"/>
        <source>All saved searches, download list and settings are deleted.</source>
        <translation>Alla sparade sökningar, nedladdningslistan och alla inställningar tas bort.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1269"/>
        <source>Latest snapshot Use it on your own risk.</source>
        <translation>Senaste snapshot. Använd på egen risk.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1155"/>
        <location filename="../newprg.ui" line="1255"/>
        <source>Delete all settings and Exit</source>
        <translation>Ta bort alla inställningsfiler och avsluta</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1158"/>
        <source>All saved searches and the list of streams to be downloaded will be deleted.</source>
        <translation>Alla sparade sökningar och listan med filer som ska laddas ner tas bort.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1204"/>
        <source>Download to Default Location</source>
        <translation>Ladda ner till standardplatsen</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="111"/>
        <location filename="../newprg.ui" line="847"/>
        <source>Search for video streams.</source>
        <oldsource>Search for video files.</oldsource>
        <translation>Sök efter videoströmmar.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="143"/>
        <source>Add current video to the list of files that will be downloaded.</source>
        <translation>Lägg till denna videoström till listan med videoströmmar som ska laddas ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="224"/>
        <source>The number of bits that are conveyed or processed per unit of time. Higher numbers give better quality and larger file..</source>
        <translation>Antalet bitar som transporteras eller behandlas per tidsenhet. Högre siffror ger bättre kvalitet och större fil ..</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="355"/>
        <source>Searches for the subtitle and downloads it at the same time as the video stream.</source>
        <translation>Söker efter undertexten och laddar ner den samtidigt som videoströmmen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="859"/>
        <source>Paste the link to the page where the video is displayed.</source>
        <translation>Klistra in länken till webbsidan där videon visas.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="874"/>
        <source>Download the stream you just searched for.</source>
        <oldsource>Download the file you just searched for.</oldsource>
        <translation>Ladda ner videoströmmen du just sökt efter.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="924"/>
        <source>View Download list</source>
        <translation>Titta på nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="927"/>
        <source>Look at the list of all the streams to download.</source>
        <oldsource>Look at the list of all the files to download.</oldsource>
        <translation>Titta på listan över alla videoströmmar som ska laddas ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="974"/>
        <source>Save the name of a video stream provider, your username and, if you want, your password.</source>
        <translation>Spara namnet på en leverantör av videoströmmar, ditt användarnamn, och om du vill, ditt lösenord.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1111"/>
        <source>Uninstall streamCapture</source>
        <translation>Avinstallera streamCapture</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1114"/>
        <source>Uninstall and remove all components</source>
        <translation>Avinstallera och ta bort alla komponenter</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1130"/>
        <source>Download after Date...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1139"/>
        <source>Stop all downloads</source>
        <translation>Stoppa alla nedladdningar</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1142"/>
        <source>Trying to stop svtplay-dl.</source>
        <translation>Försöker stoppa svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="539"/>
        <location filename="../newprg.ui" line="986"/>
        <location filename="../newprg.ui" line="1106"/>
        <source>If no saved password is found, click here.</source>
        <translation>Om inget lösenord är sparat, klicka här.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1001"/>
        <source>Searching for and downloading subtitles.</source>
        <translation>Söker efter och laddar ner undertexter.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1024"/>
        <source>Add current video to the list of streams that will be downloaded.</source>
        <oldsource>Add current video to the list of files that will be downloaded.</oldsource>
        <translation>Lägg till aktuell video till listan på videoströmmar som ska laddas ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1039"/>
        <source>Download all the streams in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</source>
        <oldsource>Download all the files in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</oldsource>
        <translation>Ladda ner alla videoströmmar i listan. Om det är samma videoström i olika kvaliteter kommer foldrar för varje videoström att skapas automatiskt.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1050"/>
        <source>Automatically create a folder for each downloaded video stream.</source>
        <translation>Skapa automatiskt en folder för varje nedladdad videoström.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1088"/>
        <source>View more information from svtplay-dl.</source>
        <translation>Visa mer information från svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="711"/>
        <source>&amp;Login</source>
        <translation>&amp;Inloggning</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="565"/>
        <source>Allows given quality to differ by an amount. 300 usually works well.</source>
        <translation>Låter given kvalitet skilja sig åt med ett belopp. 300 fungerar vanligtvis bra.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="793"/>
        <source>About...</source>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="802"/>
        <source>Check for updates...</source>
        <translation>Sök efter uppdateringar...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="826"/>
        <source>About svtplay-dl...</source>
        <translation>Om svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="835"/>
        <source>About FFmpeg...</source>
        <translation>Om FFmpeg...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="892"/>
        <source>License svtplay-dl...</source>
        <translation>Licens svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="901"/>
        <source>License FFmpeg...</source>
        <translation>Licens FFmpeg...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="915"/>
        <source>Help...</source>
        <translation>Hjälp...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="962"/>
        <source>Version history...</source>
        <translation>Versionshistorik...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1009"/>
        <source>Explain what is going on</source>
        <translation>Förklar vad som händer</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1059"/>
        <source>Edit Download list (Advanced)</source>
        <translation>Redigera nedladdningslistan (avancerat)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1062"/>
        <source>Change method or quality. Remove a download from the list. NOTE! If you change incorrectly, it will not work.</source>
        <translation>Ändra metod eller kvalitet. Ta bort en fil från nedladdning. OBS! Om du gör felaktiga ändringar kommer det inte att fungera.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1074"/>
        <source>Save Download list (Advanced)</source>
        <translation>Spara nedladdningslistan (avancerat)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1077"/>
        <source>Your changes to the download list are saved.</source>
        <translation>Dina ändringar i nedladdningslisten är sparade.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1085"/>
        <source>Show more</source>
        <translation>Visa mer</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="906"/>
        <source>Recent files</source>
        <translation>Senaste filerna</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="936"/>
        <location filename="../newprg.ui" line="944"/>
        <source>Delete download list</source>
        <translation>Ta bort nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="939"/>
        <source>All saved streams in the download list are deleted.</source>
        <oldsource>All saved items in the download list are deleted.</oldsource>
        <translation>Alla sparade videoströmmar i nedladdningslistan tas bort.</translation>
    </message>
</context>
</TS>
