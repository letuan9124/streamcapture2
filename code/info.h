//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef INFO_H
#define INFO_H

#include <QWidget>
#include <QtGlobal>
#include <sstream>
#include <string>

//EDIT
#define EXTRAFONTS false
#ifdef Q_OS_WIN
//Enable endast Windows portable
#define IS_APPIMAGE_PORTABLE
#endif
#ifdef Q_OS_LINUX
// Only AppImage, no svtplay-dl or FFmpeg
#define ONLY_STREAMCAPTURE2
#endif

#ifdef Q_OS_WIN
#define VERSION_PATH "http://bin.ceicer.com/streamcapture2/version_windows.txt"
#endif
#ifdef Q_OS_LINUX
#define VERSION_PATH "http://bin.ceicer.com/streamcapture2/version_linux.txt"
#endif
#define DOWNLOAD_PATH                                                          \
    "https://gitlab.com/posktomten/streamcapture2/-/wikis/DOWNLOADS"
#define MANUAL_PATH "https://bin.ceicer.com/streamcapture2/help-current/index_"
#define DISPLAY_NAME "streamCapture2"
#define EXECUTABLE_NAME "streamcapture2"
#define VERSION "0.29.0"
#define BUILD_DATE_TIME __DATE__ " " __TIME__
#define START_YEAR "2016"
#define CURRENT_YEAR __DATE__
#define LICENCE_VERSION "3"
#define PROGRAMMER_EMAIL "ic_0002@ceicer.com"
#define PROGRAMMER_NAME "Ingemar Ceicer"
#define PROGRAMMER_PHONE "+46706361747"
#define APPLICATION_HOMEPAGE                                                   \
    "https://gitlab.com/posktomten/streamcapture2/wikis/Home"
#define APPLICATION_HOMEPAGE_ENG                                               \
    "https://gitlab.com/posktomten/streamcapture2/wikis/Home"
#define SOURCEKODE "https://gitlab.com/posktomten/streamcapture2"
#define WIKI "https://gitlab.com/posktomten/streamcapture2/wikis/Home"
#define FFMPEG_HOMEPAGE "https://www.ffmpeg.org/";
#define SVTPLAYDL_HOMEPAGE "https://svtplay-dl.se/";
//#define ITALIAN_TRANSLATER "bovirio"
#define SIZE_OF_RECENT 50
#define RED 218
#define GREEN 255
#define BLUE 221

#ifdef Q_OS_LINUX
#define FONT 13
#endif
#ifdef Q_OS_WIN
#define FONT 10
#endif

#ifdef Q_OS_LINUX
#ifdef ONLY_STREAMCAPTURE2


#ifdef Q_PROCESSOR_X86_32 // 32 bit
#define ARG1 "http://bin.ceicer.com/zsync/streamcapture2-only-i386.AppImage.zsync"
#define ARG2 "-i streamcapture2-only-i386.AppImage"
#endif
#ifdef Q_PROCESSOR_X86_64 // 64 bit
#define ARG1 "http://bin.ceicer.com/zsync/streamcapture2-only-x86_64.AppImage.zsync"
#define ARG2 "-i streamcapture2-only-x86_64.AppImage"
#endif
#endif

#ifndef ONLY_STREAMCAPTURE2
#ifdef Q_PROCESSOR_X86_32 // 32 bit
#define ARG1 "http://bin.ceicer.com/zsync/streamcapture2-i386.AppImage.zsync"
#define ARG2 "-i streamcapture2-only-i386.AppImage"
#endif
#ifdef Q_PROCESSOR_X86_64 // 64 bit
#define ARG1 "http://bin.ceicer.com/zsync/streamcapture2-x86_64.AppImage.zsync"
#define ARG2 "-i streamcapture2-only-x86_64.AppImage"
#endif
#endif

#endif


class Info : public QWidget
{
    Q_OBJECT
public:
    void getSystem();
};

#endif // INFO_H
